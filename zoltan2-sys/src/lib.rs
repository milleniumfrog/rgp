use libc::{self, size_t};
use nalgebra_sparse::CsrMatrix;
use serde::{Deserialize, Serialize};
#[link(name = "sphynxwrapper")]
extern "C" {
    fn c_sphynx(
        row_offsets: *const size_t,
        col_indices: *const size_t,
        values: *const libc::c_double,
        rows: size_t,
        entries: size_t,
        parts: i32,
        preconditioner: i32,
        partition: *mut usize,
    ) -> i32;
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub enum SphynxPreconditioner {
    Jacobi,
    Polynomial,
    Muelu,
}

/// Executes sphynx based on f64
///
/// Expects an laplace matrix. This function does not validate that it is an
/// Laplace Matrix
///
pub fn partition_sphynx(
    laplace_matrix: &CsrMatrix<f64>,
    target_partition_count: u32,
    preconditioner: SphynxPreconditioner,
) -> Result<Vec<u32>, i32> {
    let (row_offset, col_indices, values) = laplace_matrix.csr_data();
    let mut partitioning: Vec<usize> = vec![0; laplace_matrix.nrows()];
    unsafe {
        let response = c_sphynx(
            row_offset.as_ptr(),
            col_indices.as_ptr(),
            values.as_ptr(),
            laplace_matrix.nrows(),
            laplace_matrix.nnz(),
            target_partition_count as i32,
            match preconditioner {
                SphynxPreconditioner::Jacobi => 0,
                SphynxPreconditioner::Polynomial => 1,
                SphynxPreconditioner::Muelu => 2,
            },
            partitioning.as_mut_ptr(),
        );
        if response != 0 {
            return Err(response);
        };
    }
    let partitioning = partitioning.iter().map(|x| *x as u32).collect();
    Ok(partitioning)
}
