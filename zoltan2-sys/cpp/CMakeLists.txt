# CMAKE File for "MyApp" application building against an installed Trilinos

cmake_minimum_required(VERSION 3.0)

# Define the project and the compilers
#
# NOTE: You can't call find_package(Trilinos) for a CUDA build without first
# defining the compilers.
#
project(sphynxwrapper C CXX)

# Disable Kokkos warning about not supporting C++ extensions
set(CMAKE_CXX_EXTENSIONS OFF)

# Get Trilinos as one entity but require just the packages we are using
find_package(Trilinos REQUIRED)

# Echo trilinos build info just for fun
MESSAGE("\nFound Trilinos!  Here are the details: ")
MESSAGE("   Trilinos_DIR = ${Trilinos_DIR}")
MESSAGE("   Trilinos_VERSION = ${Trilinos_VERSION}")
MESSAGE("   Trilinos_PACKAGE_LIST = ${Trilinos_PACKAGE_LIST}")
MESSAGE("   Trilinos_LIBRARIES = ${Trilinos_LIBRARIES}")
MESSAGE("   Trilinos_INCLUDE_DIRS = ${Trilinos_INCLUDE_DIRS}")
MESSAGE("   Trilinos_TPL_LIST = ${Trilinos_TPL_LIST}")
MESSAGE("   Trilinos_TPL_LIBRARIES = ${Trilinos_TPL_LIBRARIES}")
MESSAGE("   Trilinos_BUILD_SHARED_LIBS = ${Trilinos_BUILD_SHARED_LIBS}")
MESSAGE("End of Trilinos details\n")

include_directories("../trilinos/packages/anasazi/src")

# Build the APP and link to Trilinos
# add_executable(sphynxwrapper_main ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp)
add_library(sphynxwrapper STATIC ${CMAKE_CURRENT_SOURCE_DIR}/sphynxwrapper.cpp)
target_link_libraries(sphynxwrapper PRIVATE Trilinos::all_libs)
# target_link_libraries(sphynxwrapper_main PRIVATE Trilinos::all_libs)
# Or, above could have linked to just Tpetra::all_libs

MESSAGE("Target dir ${CMAKE_INSTALL_PREFIX}")
install(TARGETS sphynxwrapper DESTINATION ${CMAKE_INSTALL_PREFIX}/lib)