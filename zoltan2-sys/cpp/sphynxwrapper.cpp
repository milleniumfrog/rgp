#include <Zoltan2_PartitioningSolution.hpp>
#include <Zoltan2_PartitioningProblem.hpp>
#include <Zoltan2_MatrixAdapter.hpp>
#include <Zoltan2_BasicVectorAdapter.hpp>
#include <Zoltan2_InputTraits.hpp>
#include <Tpetra_Map.hpp>
#include <Tpetra_CrsMatrix.hpp>
#include <vector>
#include <Zoltan2_SphynxProblem.hpp>
#include <cstdlib>
#include <iostream>
#include <Tpetra_Core.hpp>
#include <Teuchos_Comm.hpp>
#include <Teuchos_RCP.hpp>

extern "C" {
  /** rcb geometric partitioniert based on */
  int c_sphynx(
    size_t *row_offsets,
    size_t *col_indices,
    double *values,
    size_t nrows,
    size_t non_zero_entry_count,
    int32_t nparts,
    int32_t preconditioner,
    size_t *partitioning_out
  ) {
    try {
      using Teuchos::RCP;

      auto precond = "jacobi";

      if (preconditioner == 1) {
        precond = "polynomial";
      }
      else if (preconditioner == 2) {
        precond = "muelu";
      }

      typedef Tpetra::Map<> Map_t;
      typedef Tpetra::Vector<>::global_ordinal_type global_ordinal_t;
      typedef Tpetra::Vector<>::local_ordinal_type local_ordinal_t;
      typedef Tpetra::CrsMatrix<> CsrMatrix_t;
      typedef Teuchos::Array<global_ordinal_t> IndexArray_t;
      typedef Teuchos::Array<double> ValueArray_t;


      // init single node mpi communication and set distribution
      RCP<const Teuchos::Comm<int> > comm = Tpetra::getDefaultComm();


      // Teuchos::RCP<Tpetra::Map<int, int, Tpetra::KokkosCompat::KokkosSerialWrapperNode>> map = rcp(new Tpetra::Map<int, int, Tpetra::KokkosCompat::KokkosSerialWrapperNode>(100, 0, comm));
      RCP<const Map_t> map = rcp(new Map_t(nrows, 0, comm, Tpetra::LocallyReplicated));


      // wrap csr data in techos data structures
      auto tril_col_indices = rcp(new IndexArray_t(non_zero_entry_count, 0));
      auto tril_values = rcp(new ValueArray_t(non_zero_entry_count, 0));

      auto tril_col_indices_begin = tril_col_indices->begin();
      auto tril_values_begin = tril_values->begin();
      for (size_t i = 0; i < non_zero_entry_count; ++i) {
        tril_col_indices->operator[](i) = col_indices[i];
        tril_values->operator[](i)= values[i];
      }

      size_t max_diff = 0;
      for (int i = 0; i < nrows; ++i) {
        size_t diff = row_offsets[i+1] - row_offsets[i];
        if (diff > max_diff) {
          max_diff = diff;
        }
      }

      // create csr matrix and fill it with data
      RCP<CsrMatrix_t> matrix = rcp(new CsrMatrix_t(map, max_diff)); // TODO set max or optimize

      for (size_t row = 0; row < nrows; ++ row) {
        size_t row_offset_start = row_offsets[row];
        size_t row_offset_end = row_offsets[row + 1];
        matrix->insertGlobalValues(
          row,
          tril_col_indices->view(
            row_offset_start,
            row_offset_end - row_offset_start
          ),
          tril_values->view(
            row_offset_start,
            row_offset_end - row_offset_start
          )
        );
      }
      matrix->fillComplete();

      // create sphynx data
      using GraphAdapter = Zoltan2::XpetraCrsGraphAdapter<Tpetra::CrsGraph<>>;
      auto graph = matrix->getCrsGraph();
      GraphAdapter adapter = GraphAdapter(graph);

      // set partitioning parameters
      auto params = Teuchos::ParameterList(); // TODO
      params.set("num_global_parts", nparts);
      RCP<Teuchos::ParameterList> sphynxParams = rcp(new Teuchos::ParameterList()); // TODO
      sphynxParams->set("sphynx_skip_preprocessing", true);
      sphynxParams->set("sphynx_preconditioner_type", precond);
      sphynxParams->set("sphynx_problem_type", "combinatorial");
      sphynxParams->set("sphynx_tolerance", 0.0001);

      // perform partitioning
      Zoltan2::SphynxProblem<GraphAdapter> problem(&adapter, &params, sphynxParams);
      problem.solve();
      auto parts = problem.getSolution().getPartListView();

      // copy partitioning data in out partitioning_out
      for (int i = 0; i < nrows; i++) {
        partitioning_out[i] = parts[i];
      }
    }
    catch(std::runtime_error e) {
      std::cerr << "Runtime Error ::" << e.what() << std::endl;
      return 2;
    }
    catch (std::exception e) {
      std::cerr << typeid(e).name() << std::endl;
      std::cerr << "Error :: " << e.what() << std::endl;
      std::cerr << "You might need to reproduce the problem in C++ to see the complete error message." << std::endl;
      return 1;
    }
    return 0;
  }
}