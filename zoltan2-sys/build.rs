use cmake::Config;
use std::{env, path::PathBuf};

/// builds zoltan2 as static libraries with cmake and registers all libraries for the static the compiler
///
///
fn main() {
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());

    // build zoltan2
    let _dst = Config::new("./trilinos")
        .generator("Ninja")
        .define("BUILD_SHARED_LIBS:BOOL", "OFF")
        .define("Trilinos_ENABLE_Zoltan2:BOOL", "ON")
        .define("Trilinos_ENABLE_Zoltan2Sphynx:BOOL", "ON")
        // .define("Trilinos_ENABLE_Anasazi:BOOL", "ON")
        // .define("Trilinos_ENABLE_MueLu:BOOL", "ON")
        // .define("Trilinos_ENABLE_Amesos2:BOOL", "ON")
        // .define("Trilinos_ENABLE_INSTALL_CMAKE_CONFIG_FILES:BOOL", "ON")
        .out_dir(format!("{}/zoltan2", out_path.display()))
        .build();

    // link zoltan2
    println!(
        "cargo:rustc-link-search=native={}/zoltan2/lib",
        out_path.display()
    );

    println!("cargo:rustc-link-lib=dylib=c++");
    println!("cargo:rustc-link-lib=dylib=cblas");
    println!("cargo:rustc-link-lib=dylib=lapack");

    let zoltan_lib_dir = std::fs::read_dir(format!("{}/zoltan2/lib", out_path.display()));

    for entry in zoltan_lib_dir.unwrap() {
        let entry = entry.unwrap();
        let path = entry.path();
        if path.is_file() {
            let file_name = path.file_name().unwrap().to_str().unwrap().to_owned();
            if file_name.starts_with("lib") && file_name.ends_with(".a") {
                println!(
                    "cargo:rustc-link-lib=static={}",
                    file_name
                        .strip_prefix("lib")
                        .unwrap()
                        .strip_suffix(".a")
                        .unwrap()
                );
            }
        }
    }

    // build zoltan2 c wrapper
    let _dst = Config::new("./cpp")
        .define("BUILD_SHARED_LIBS:BOOL", "OFF")
        .define(
            "CMAKE_PREFIX_PATH",
            format!("{}/zoltan2", out_path.display()),
        )
        .out_dir(format!("{}/sphynxwrapper", out_path.display()))
        .build();

    // link zoltan2 wrapper
    println!(
        "cargo:rustc-link-search=native={}/sphynxwrapper/lib",
        out_path.display()
    );

    println!("cargo:rustc-link-lib=static=sphynxwrapper");

    // register rerun flags
    println!("cargo:rerun-if-changed=src/lib.rs");
    println!("cargo:rerun-if-changed=cpp/sphynxwrapper.cpp");
}
