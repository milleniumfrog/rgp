use bindgen::Builder;
use cmake::Config;
use std::{env, path::PathBuf};

fn main() {
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());

    let dst = Config::new("arpack-ng")
        .define("ICB", "ON")
        .define("BUILD_SHARED_LIBS", "OFF")
        .define("BUILD_TESTING", "OFF")
        .define("CMAKE_Fortran_FLAGS", "-fPIC")
        .build();
    let bindings = Builder::default()
        .header(&format!("{}/include/arpack/arpack.h", dst.display()))
        .generate()
        .expect("Could not generate bindings.");
    bindings
        .write_to_file(out_path.join("arpack.rs"))
        .expect("Could not write bindings!");

    println!("cargo:rustc-link-search=native={}/lib", dst.display());
    println!("cargo:rustc-link-search=native=/opt/homebrew/Cellar/gcc/13.2.0/lib/gcc/13");
    println!("cargo:rustc-link-lib=static=arpack");
    println!("cargo:rustc-link-lib=dylib=cblas");
    println!("cargo:rustc-link-lib=dylib=gfortran");
    println!("cargo:rustc-link-lib=dylib=lapack");
}
