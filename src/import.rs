use std::{
    fs::File,
    io::{self, BufRead},
    str::FromStr,
};

use nalgebra::DMatrix;
use nalgebra_sparse::{CooMatrix, CsrMatrix};

#[derive(Debug)]
pub enum LoadMatrixError {
    Open { error: std::io::Error },
    EmptyFile,
    ParseFloat(<f64 as FromStr>::Err),
    ParseInt(<i32 as FromStr>::Err),
}

impl From<std::io::Error> for LoadMatrixError {
    fn from(value: std::io::Error) -> Self {
        LoadMatrixError::Open { error: value }
    }
}

pub fn import_matrix_from_file(file_path: &String) -> Result<CsrMatrix<f64>, LoadMatrixError> {
    let file = File::open(file_path.clone())?;
    let mut buf_reader = io::BufReader::new(file).lines();
    // read first line and check if import is symmetric
    let mut line = match buf_reader.next() {
        Some(line) => line?,
        None => return Err(LoadMatrixError::EmptyFile),
    };
    let is_symmetric = line.contains("symmetric");
    // skip all comments at the start of the file
    loop {
        line = match buf_reader.next() {
            Some(line) => line?,
            None => return Err(LoadMatrixError::EmptyFile),
        };
        if !line.starts_with("%") {
            break;
        }
    }
    // read the expected matrix size
    let line_values: Vec<String> = line.split(" ").map(|x| x.to_owned()).collect();
    let n: usize = match line_values[0].parse() {
        Ok(n) => n,
        Err(e) => return Err(LoadMatrixError::ParseInt(e)),
    };
    // construct matrix
    let mut construction_matrix: CooMatrix<f64> = CooMatrix::new(n, n);
    for line in buf_reader {
        let line = line?;
        if line.starts_with("%") {
            continue;
        }
        let line_values: Vec<String> = line.split(" ").map(|x| x.to_owned()).collect();
        let i: usize = match line_values[0].parse() {
            Ok(i) => i,
            Err(e) => return Err(LoadMatrixError::ParseInt(e)),
        };
        let i: usize = i - 1;
        let j: usize = match line_values[1].parse() {
            Ok(j) => j,
            Err(e) => return Err(LoadMatrixError::ParseInt(e)),
        };
        let j = j - 1;
        let v: f64 = match line_values.get(2) {
            None => 1.0,
            Some(v) => match v.parse() {
                Ok(v) => v,
                Err(e) => return Err(LoadMatrixError::ParseFloat(e)),
            },
        };
        construction_matrix.push(i, j, v);
    }
    let csr = {
        let csr = CsrMatrix::from(&construction_matrix);
        if is_symmetric {
            csr.transpose() + &csr - csr.diagonal_as_csr()
        } else {
            csr
        }
    };
    return Ok(csr);
}

pub fn import_coordinates_from_file(file_path: &String) -> Result<DMatrix<f64>, LoadMatrixError> {
    let file = File::open(file_path.clone())?;
    let mut buf_reader = io::BufReader::new(file).lines();
    // read first line
    let mut line: String;
    // skip all comments at the start of the file
    loop {
        line = match buf_reader.next() {
            Some(line) => line?,
            None => return Err(LoadMatrixError::EmptyFile),
        };
        if !line.starts_with("%") {
            break;
        }
    }
    // read dimensions
    let line_values: Vec<String> = line.split(" ").map(|x| x.to_owned()).collect();
    let n: usize = match line_values[0].parse() {
        Ok(n) => n,
        Err(e) => return Err(LoadMatrixError::ParseInt(e)),
    };
    let m: usize = match line_values[1].parse() {
        Ok(m) => m,
        Err(e) => return Err(LoadMatrixError::ParseInt(e)),
    };
    // load coordinates
    let mut matrix: DMatrix<f64> = DMatrix::zeros(n, m);
    for (counter, line) in buf_reader.enumerate() {
        let line = line?;
        if line.starts_with("%") {
            continue;
        }
        let n_index = counter % n;
        let m_index = counter / n;
        let v: f64 = match line.parse() {
            Ok(v) => v,
            Err(e) => return Err(LoadMatrixError::ParseFloat(e)),
        };
        matrix[(n_index, m_index)] = v;
    }
    return Ok(matrix);
}

pub fn get_matrix_without_diag(matrix: &CsrMatrix<f64>) -> CsrMatrix<f64> {
    let mut construction_matrix = CooMatrix::new(matrix.nrows(), matrix.nrows());
    for (x, y, v) in matrix.triplet_iter() {
        if x != y {
            construction_matrix.push(x, y, *v);
        }
    }
    let csr = CsrMatrix::from(&construction_matrix);
    return csr;
}

pub fn make_matrix_symmetric(matrix: &CsrMatrix<f64>) -> CsrMatrix<f64> {
    return (matrix.transpose() + matrix) / 2.0;
}
