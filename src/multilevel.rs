pub mod coarsen;
pub mod refine;

use debug_print::debug_println;
use nalgebra_sparse::CsrMatrix;

pub use metis_sys::{partition_metis_kway, partition_metis_rb};

use crate::{
    apply_component_partition, extract_components_for_partition,
    growing::{
        graph_growing_bisection, greedy_graph_growing_bisection, GraphGrowingBisectionAlgorithm,
        GraphGrowingBisectionError, GraphGrowingError,
    },
    helpers::{get_sub_graph, get_weighted_inbalance},
    iterativeimprovement::{partition_bkl, partition_fm, partition_kl},
    spectral::{spectral_bisection, SpectralBisectionError},
};

use self::{
    coarsen::{
        coarsen_2hop_heavy_edge_matching, coarsen_first_matching, coarsen_heavy_edge_matching,
        CoarsenAlgorithm,
    },
    refine::{refine_interpolate, RefineAlgorithm},
};

#[derive(Debug, Clone)]
pub struct CoarsenData {
    pub graph: CsrMatrix<f64>,
    pub vertex_weights: Vec<f64>,
    pub labels: Vec<usize>,
}

#[derive(Debug)]
pub enum MultilevelSpectralRbError {
    TargetPartitionCountNotPowerOfTwo,
    MultilevelSpectralBisectionError(MultilevelSpectralBisectionError),
}

impl From<MultilevelSpectralBisectionError> for MultilevelSpectralRbError {
    fn from(value: MultilevelSpectralBisectionError) -> Self {
        return MultilevelSpectralRbError::MultilevelSpectralBisectionError(value);
    }
}

pub fn partition_multilevel_spectral_rb(
    ca: &CoarsenAlgorithm,
    ra: &RefineAlgorithm,
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_partition_count: u32,
) -> Result<Vec<u32>, MultilevelSpectralRbError> {
    if !target_partition_count.is_power_of_two() {
        return Err(MultilevelSpectralRbError::TargetPartitionCountNotPowerOfTwo);
    }

    let target_depth = u32::ilog2(target_partition_count);

    let partition =
        internal_multilevel_spectral_rb(ca, ra, &graph, &vertex_weights, 0, target_depth)?;
    return Ok(partition);
}

fn internal_multilevel_spectral_rb(
    ca: &CoarsenAlgorithm,
    ra: &RefineAlgorithm,
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    depth: u32,
    target_depth: u32,
) -> Result<Vec<u32>, MultilevelSpectralRbError> {
    let initial_partition = multilevel_spectral_bisection(&ca, &ra, &graph, &vertex_weights, None)?;
    let partition = if depth < target_depth - 1 {
        let (subgraph_a, vertex_weights_a) =
            get_sub_graph(&graph, &vertex_weights, &initial_partition, 0);
        let (subgraph_b, vertex_weights_b) =
            get_sub_graph(&graph, &vertex_weights, &initial_partition, 1);
        let partition_a = internal_multilevel_spectral_rb(
            &ca,
            &ra,
            &subgraph_a,
            &vertex_weights_a,
            depth + 1,
            target_depth,
        )?;
        let partition_b = internal_multilevel_spectral_rb(
            &ca,
            &ra,
            &subgraph_b,
            &vertex_weights_b,
            depth + 1,
            target_depth,
        )?;
        // combine the recursive partitions
        let mut tmp_partition: Vec<u32> = vec![0; initial_partition.len()];
        let mut partition_a_counter = 0;
        let mut partition_b_counter = 0;
        for i in 0..initial_partition.len() {
            if initial_partition[i] == 0 {
                tmp_partition[i] = partition_a[partition_a_counter];
                partition_a_counter += 1;
            } else {
                tmp_partition[i] = partition_b[partition_b_counter]
                    + ((2 as u32)
                        .pow(target_depth.abs_diff(depth).abs_diff(1).try_into().unwrap()));
                partition_b_counter += 1;
            }
        }
        tmp_partition
    } else {
        initial_partition
    };
    return Ok(partition);
}

#[derive(Debug)]
pub enum MultilevelSpectralBisectionError {
    InvalidTargetWeight,
    SpectralBisectionError(SpectralBisectionError),
}

impl From<SpectralBisectionError> for MultilevelSpectralBisectionError {
    fn from(value: SpectralBisectionError) -> Self {
        return MultilevelSpectralBisectionError::SpectralBisectionError(value);
    }
}

pub fn multilevel_spectral_bisection(
    ca: &CoarsenAlgorithm,
    ra: &RefineAlgorithm,
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_weight: Option<f64>,
) -> Result<Vec<u32>, MultilevelSpectralBisectionError> {
    let total_weight: f64 = vertex_weights.iter().sum();
    let target_weight = target_weight.unwrap_or(total_weight / 2.0);
    let graph_size = graph.nrows();
    if target_weight < 0.0 || target_weight > total_weight {
        return Err(MultilevelSpectralBisectionError::InvalidTargetWeight);
    }

    let components_for_bipartition =
        extract_components_for_partition(&graph, &vertex_weights, target_weight);
    let component_to_partition = &components_for_bipartition.component_to_partition;
    let new_target_weight = components_for_bipartition.new_target_weight;

    println!("Coarsen");
    let coarsen_data = match ca {
        CoarsenAlgorithm::FirstMatch {
            target_size,
            max_iterations,
        } => coarsen_first_matching(
            &component_to_partition.graph,
            &component_to_partition.vertex_weights,
            *target_size,
            *max_iterations,
        ),
        CoarsenAlgorithm::HeavyEdgeMatch {
            target_size,
            max_iterations,
        } => coarsen_heavy_edge_matching(
            &component_to_partition.graph,
            &component_to_partition.vertex_weights,
            *target_size,
            *max_iterations,
        ),
        CoarsenAlgorithm::HeavyEdgeMatch2Hop {
            target_size,
            max_iterations,
        } => coarsen_2hop_heavy_edge_matching(
            &component_to_partition.graph,
            &component_to_partition.vertex_weights,
            *target_size,
            *max_iterations,
        ),
    };

    let mut coarsen_data_iter = coarsen_data.iter();
    let coarsed = coarsen_data_iter.next().unwrap();
    let coarsed_graph = &coarsed.graph;
    let coarsed_vertex_weights = &coarsed.vertex_weights;

    println!("Initial Partition");
    let mut component_partition = spectral_bisection(
        coarsed_graph,
        coarsed_vertex_weights,
        Some(new_target_weight),
    )?;

    debug_println!(
        "[MULTILEVEL] -> spectral bisection: inbalance {}",
        get_weighted_inbalance(coarsed_vertex_weights, &component_partition, 2)
    );

    println!("Refine");
    for c in coarsen_data_iter {
        component_partition = match ra {
            RefineAlgorithm::Interpolate => refine_interpolate(&c.labels, &component_partition),
            RefineAlgorithm::KL { max_iterations } => {
                component_partition = refine_interpolate(&c.labels, &component_partition);
                partition_kl(&c.graph, &component_partition, *max_iterations)
            }
            RefineAlgorithm::BKL { max_iterations } => {
                component_partition = refine_interpolate(&c.labels, &component_partition);
                partition_bkl(&c.graph, &component_partition, *max_iterations)
            }
            RefineAlgorithm::FM => {
                let component_partition = refine_interpolate(&c.labels, &component_partition);
                partition_fm(&c.graph, &component_partition, &c.vertex_weights)
            }
        }
    }

    let partition = apply_component_partition(
        graph_size,
        components_for_bipartition.components_in_subsection,
        &component_to_partition.vertex_ids,
        &component_partition,
    );

    Ok(partition)
}

#[derive(Debug)]
pub enum MultilevelGraphGrowingRbError {
    TargetPartitionCountNotPowerOfTwo,
    MultilevelGraphGrowingBisectionError(MultilevelGraphGrowingBisectionError),
}

impl From<MultilevelGraphGrowingBisectionError> for MultilevelGraphGrowingRbError {
    fn from(value: MultilevelGraphGrowingBisectionError) -> Self {
        return MultilevelGraphGrowingRbError::MultilevelGraphGrowingBisectionError(value);
    }
}

pub fn partition_multilevel_graph_growing_rb(
    ca: &CoarsenAlgorithm,
    ga: &GraphGrowingBisectionAlgorithm,
    ra: &RefineAlgorithm,
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_partition_count: u32,
) -> Result<Vec<u32>, MultilevelGraphGrowingRbError> {
    if !target_partition_count.is_power_of_two() {
        return Err(MultilevelGraphGrowingRbError::TargetPartitionCountNotPowerOfTwo);
    }

    let target_depth = u32::ilog2(target_partition_count);

    let partition =
        internal_multilevel_graph_growing_rb(ca, ga, ra, &graph, &vertex_weights, 0, target_depth)?;
    return Ok(partition);
}

fn internal_multilevel_graph_growing_rb(
    ca: &CoarsenAlgorithm,
    ga: &GraphGrowingBisectionAlgorithm,
    ra: &RefineAlgorithm,
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    depth: u32,
    target_depth: u32,
) -> Result<Vec<u32>, MultilevelGraphGrowingRbError> {
    let initial_partition =
        multilevel_graph_growing_bisection(&ca, &ga, &ra, &graph, &vertex_weights, None)?;
    let partition = if depth < target_depth - 1 {
        let (subgraph_a, vertex_weights_a) =
            get_sub_graph(&graph, &vertex_weights, &initial_partition, 0);
        let (subgraph_b, vertex_weights_b) =
            get_sub_graph(&graph, &vertex_weights, &initial_partition, 1);
        let partition_a = internal_multilevel_graph_growing_rb(
            &ca,
            &ga,
            &ra,
            &subgraph_a,
            &vertex_weights_a,
            depth + 1,
            target_depth,
        )?;
        let partition_b = internal_multilevel_graph_growing_rb(
            &ca,
            &ga,
            &ra,
            &subgraph_b,
            &vertex_weights_b,
            depth + 1,
            target_depth,
        )?;
        // combine the recursive partitions
        let mut tmp_partition: Vec<u32> = vec![0; initial_partition.len()];
        let mut partition_a_counter = 0;
        let mut partition_b_counter = 0;
        for i in 0..initial_partition.len() {
            if initial_partition[i] == 0 {
                tmp_partition[i] = partition_a[partition_a_counter];
                partition_a_counter += 1;
            } else {
                tmp_partition[i] = partition_b[partition_b_counter]
                    + ((2 as u32)
                        .pow(target_depth.abs_diff(depth).abs_diff(1).try_into().unwrap()));
                partition_b_counter += 1;
            }
        }
        tmp_partition
    } else {
        initial_partition
    };
    return Ok(partition);
}

#[derive(Debug)]
pub enum MultilevelGraphGrowingBisectionError {
    InvalidTargetWeight,
    GraphGrowingBisectionError(GraphGrowingBisectionError),
}

impl From<GraphGrowingBisectionError> for MultilevelGraphGrowingBisectionError {
    fn from(value: GraphGrowingBisectionError) -> Self {
        return MultilevelGraphGrowingBisectionError::GraphGrowingBisectionError(value);
    }
}

pub fn multilevel_graph_growing_bisection(
    ca: &CoarsenAlgorithm,
    ga: &GraphGrowingBisectionAlgorithm,
    ra: &RefineAlgorithm,
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_weight: Option<f64>,
) -> Result<Vec<u32>, MultilevelGraphGrowingBisectionError> {
    let total_weight: f64 = vertex_weights.iter().sum();
    let target_weight = target_weight.unwrap_or(total_weight / 2.0);
    let graph_size = graph.nrows();
    if target_weight < 0.0 || target_weight > total_weight {
        return Err(MultilevelGraphGrowingBisectionError::InvalidTargetWeight);
    }

    let components_for_bipartition =
        extract_components_for_partition(&graph, &vertex_weights, target_weight);
    let component_to_partition = &components_for_bipartition.component_to_partition;
    let new_target_weight = components_for_bipartition.new_target_weight;

    println!("Coarsen");
    let coarsen_data = match ca {
        CoarsenAlgorithm::FirstMatch {
            target_size,
            max_iterations,
        } => coarsen_first_matching(
            &component_to_partition.graph,
            &component_to_partition.vertex_weights,
            *target_size,
            *max_iterations,
        ),
        CoarsenAlgorithm::HeavyEdgeMatch {
            target_size,
            max_iterations,
        } => coarsen_heavy_edge_matching(
            &component_to_partition.graph,
            &component_to_partition.vertex_weights,
            *target_size,
            *max_iterations,
        ),
        CoarsenAlgorithm::HeavyEdgeMatch2Hop {
            target_size,
            max_iterations,
        } => coarsen_2hop_heavy_edge_matching(
            &component_to_partition.graph,
            &component_to_partition.vertex_weights,
            *target_size,
            *max_iterations,
        ),
    };

    let mut coarsen_data_iter = coarsen_data.iter();
    let coarsed = coarsen_data_iter.next().unwrap();
    let coarsed_graph = &coarsed.graph;
    let coarsed_vertex_weights = &coarsed.vertex_weights;

    println!(
        "Initial Partition with coarsed_graph size {} in coarse steps {}",
        coarsed_graph.nrows(),
        coarsen_data.len()
    );
    let mut component_partition = match ga {
        GraphGrowingBisectionAlgorithm::GGP { iterations } => graph_growing_bisection(
            &coarsed_graph,
            &coarsed_vertex_weights,
            *iterations,
            Some(new_target_weight),
        )?,
        GraphGrowingBisectionAlgorithm::GGGP { iterations } => greedy_graph_growing_bisection(
            &coarsed_graph,
            &coarsed_vertex_weights,
            *iterations,
            Some(new_target_weight),
        )?,
    };
    debug_println!(
        "[MULTILEVEL] -> graph growing bisection: inbalance {}",
        get_weighted_inbalance(coarsed_vertex_weights, &component_partition, 2)
    );

    let mut counter = 0;
    for c in coarsen_data_iter {
        println!("Refine iteration {}", counter);
        counter += 1;
        component_partition = match ra {
            RefineAlgorithm::Interpolate => refine_interpolate(&c.labels, &component_partition),
            RefineAlgorithm::KL { max_iterations } => {
                component_partition = refine_interpolate(&c.labels, &component_partition);
                partition_kl(&c.graph, &component_partition, *max_iterations)
            }
            RefineAlgorithm::BKL { max_iterations } => {
                component_partition = refine_interpolate(&c.labels, &component_partition);
                partition_bkl(&c.graph, &component_partition, *max_iterations)
            }
            RefineAlgorithm::FM => {
                let component_partition = refine_interpolate(&c.labels, &component_partition);
                partition_fm(&c.graph, &component_partition, &c.vertex_weights)
            }
        }
    }

    let partition = apply_component_partition(
        graph_size,
        components_for_bipartition.components_in_subsection,
        &component_to_partition.vertex_ids,
        &component_partition,
    );

    Ok(partition)
}
