use std::collections::HashSet;

use nalgebra_sparse::{CooMatrix, CsrMatrix, SparseEntry};

use crate::Component;

/// Checks if an graph is connected
pub fn is_graph_connected(graph: &CsrMatrix<f64>) -> bool {
    let mut visited_vertices: Vec<bool> = vec![false; graph.nrows()];

    let mut next_visit_buffer: Vec<usize> = vec![0];

    while !next_visit_buffer.is_empty() {
        let tmp_visit_buffer = next_visit_buffer.clone();
        next_visit_buffer = Vec::new();
        for row_index in tmp_visit_buffer {
            if !visited_vertices[row_index] {
                visited_vertices[row_index] = true;
                let row = graph.get_row(row_index).unwrap();
                let neighbors = row.col_indices();
                for n in neighbors {
                    if !visited_vertices[*n] {
                        next_visit_buffer.push(*n);
                    }
                }
            }
        }
    }
    return visited_vertices
        .iter()
        .cloned()
        .reduce(|acc, e| acc && e)
        .unwrap();
}

pub fn get_components_from_graph(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
) -> Vec<Component> {
    let mut visited_vertices: Vec<bool> = vec![false; graph.nrows()];
    let mut component_vertices_list = Vec::new();
    for i in 0..visited_vertices.len() {
        if !visited_vertices[i] {
            let mut component_vertices: HashSet<usize> = HashSet::new();
            component_vertices.insert(i);
            let mut next_visit_buffer: Vec<usize> = vec![i];
            while !next_visit_buffer.is_empty() {
                let row_index = next_visit_buffer.pop().unwrap();
                if !visited_vertices[row_index] {
                    visited_vertices[row_index] = true;
                    component_vertices.insert(row_index);
                    let row = graph.get_row(row_index).unwrap();
                    let neighbors = row.col_indices();
                    for n in neighbors {
                        if !visited_vertices[*n] {
                            next_visit_buffer.push(*n);
                        }
                    }
                }
            }
            component_vertices_list.push(component_vertices);
        }
    }
    let mut component_list = Vec::new();
    for component_vertices in component_vertices_list {
        let mut partition = vec![0; graph.nrows()];
        for cv in component_vertices.iter() {
            partition[*cv] = 1;
        }
        let subgraph = get_sub_graph(&graph, &vertex_weights, &partition, 1);
        component_list.push(Component {
            graph: subgraph.0,
            vertex_weights: subgraph.1,
            vertex_ids: component_vertices,
        });
        component_list.sort_by(|a, b| a.graph.nrows().cmp(&b.graph.nrows()));
    }
    return component_list;
}

pub fn get_laplace_matrix(matr: &CsrMatrix<f64>) -> CsrMatrix<f64> {
    let mut coo: CooMatrix<f64> = CooMatrix::new(matr.nrows(), matr.nrows());
    for row_index in 0..matr.nrows() {
        let row = matr.get_row(row_index).unwrap();
        let mut neg_row_sum = 0.0;
        for col_index in row.col_indices() {
            if row_index == *col_index {
                continue;
            }
            if let SparseEntry::NonZero(v) = row.get_entry(*col_index).unwrap() {
                neg_row_sum += (*v).abs();
                coo.push(row_index, *col_index, -v.abs());
            }
        }
        coo.push(row_index, row_index, neg_row_sum);
    }
    return CsrMatrix::from(&coo);
}

pub fn get_sub_graph(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    partition: &Vec<u32>,
    subgraph_id: u32,
) -> (CsrMatrix<f64>, Vec<f64>) {
    let mut size_counter: usize = 0;
    let mut mapping: Vec<usize> = vec![0; partition.len()];
    for i in 0..partition.len() {
        if partition[i] == subgraph_id {
            mapping[i] = size_counter;
            size_counter += 1;
        }
    }

    let mut new_coo_graph: CooMatrix<f64> = CooMatrix::new(size_counter, size_counter);
    let mut new_vertex_weights = vec![0.0; size_counter];

    let mut counter: usize = 0;
    // assign the new vertex weights
    for i in 0..partition.len() {
        if partition[i] == subgraph_id {
            new_vertex_weights[counter] = vertex_weights[i];
            counter += 1;
        }
    }

    // create new graph in csr form
    for (x, y, v) in graph.triplet_iter() {
        if partition[x] == subgraph_id && partition[y] == subgraph_id {
            new_coo_graph.push(mapping[x], mapping[y], *v);
        }
    }
    return (CsrMatrix::from(&new_coo_graph), new_vertex_weights);
}

pub fn get_edgecut_weight(graph: &CsrMatrix<f64>, partition: &Vec<u32>) -> f64 {
    let mut weight = 0.0;
    for (i, j, v) in graph.triplet_iter() {
        if partition[i] != partition[j] {
            weight += *v;
        }
    }
    return weight / 2.0;
}

pub fn get_weighted_inbalance(
    vertex_weights: &Vec<f64>,
    partition: &Vec<u32>,
    partition_count: u32,
) -> f64 {
    let total_weight = vertex_weights.iter().sum::<f64>();
    let expected_weight = total_weight / (partition_count as f64);
    let mut weight_counter = vec![0.0; partition_count as usize];
    for (i, p) in partition.iter().enumerate() {
        weight_counter[*p as usize] += vertex_weights[i];
    }
    let mut inbalance = 0.0;
    for b_1 in weight_counter.iter() {
        inbalance = f64::max(inbalance, f64::abs(1.0 - (b_1 / expected_weight)));
    }
    return inbalance;
}
