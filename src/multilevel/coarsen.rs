use std::collections::HashMap;

use debug_print::debug_println;
use nalgebra_sparse::{CooMatrix, CsrMatrix};
use rand::seq::SliceRandom;
use serde::{Deserialize, Serialize};

use super::CoarsenData;

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum CoarsenAlgorithm {
    FirstMatch {
        target_size: usize,
        max_iterations: usize,
    },
    HeavyEdgeMatch {
        target_size: usize,
        max_iterations: usize,
    },
    HeavyEdgeMatch2Hop {
        target_size: usize,
        max_iterations: usize,
    },
}

pub fn coarsen_first_matching(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_size: usize,
    max_iterations: usize,
) -> Vec<CoarsenData> {
    let mut coarsen_steps: Vec<CoarsenData> = Vec::new();

    let mut curr_graph = graph.clone();
    let mut curr_vertex_weights = vertex_weights.clone();
    for _ in 0..max_iterations {
        let rows = curr_graph.nrows();
        if curr_graph.nrows() < target_size {
            break;
        }

        let mut label_counter: usize = 0;
        let mut maybe_labels: Vec<Option<usize>> = vec![None; rows];

        for row_id in 0..rows {
            // if the node is already labeled, do nothing, else add new label
            if let None = maybe_labels[row_id] {
                // set new label
                let new_label = label_counter;
                label_counter += 1;
                maybe_labels[row_id] = Some(new_label);
                // check for unlabeld neighbors and add the new_label
                let row = curr_graph.get_row(row_id).unwrap();
                let neighbors = row.col_indices().to_owned();
                for n in neighbors {
                    if let None = maybe_labels[n] {
                        maybe_labels[n] = Some(new_label);
                        break;
                    }
                }
            }
        }
        let labels: Vec<usize> = maybe_labels.iter().map(|x| x.unwrap()).collect();
        let (coarsed_graph, new_vertex_weights) = generate_new_matrix_from_labels(
            &curr_graph,
            &curr_vertex_weights,
            &labels,
            label_counter,
        );
        coarsen_steps.push(CoarsenData {
            graph: curr_graph,
            vertex_weights: curr_vertex_weights,
            labels,
        });
        curr_graph = coarsed_graph;
        curr_vertex_weights = new_vertex_weights;
    }
    coarsen_steps.push(CoarsenData {
        graph: curr_graph,
        vertex_weights: curr_vertex_weights,
        labels: Vec::new(),
    });
    coarsen_steps.reverse();
    debug_println!(
        "[COARSEN] -> first match: coarsen steps {}",
        coarsen_steps.len()
    );
    coarsen_steps
}

pub fn coarsen_heavy_edge_matching(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_size: usize,
    max_iterations: usize,
) -> Vec<CoarsenData> {
    let mut thread_rng = rand::thread_rng();
    let mut coarsen_steps: Vec<CoarsenData> = Vec::new();

    let mut curr_graph = graph.clone();
    let mut curr_vertex_weights = vertex_weights.clone();
    for _ in 0..max_iterations {
        let rows = curr_graph.nrows();
        if curr_graph.nrows() < target_size {
            break;
        }

        let mut label_counter: usize = 0;
        let mut maybe_labels: Vec<Option<usize>> = vec![None; rows];

        let mut row_order = Vec::from_iter(0..rows);
        row_order.shuffle(&mut thread_rng);
        for row_id in row_order {
            // if the node is already labeled, do nothing, else add new label
            if let None = maybe_labels[row_id] {
                // set new label
                let new_label = label_counter;
                label_counter += 1;
                maybe_labels[row_id] = Some(new_label);
                // check for unlabeld neighbors and add the new_label
                let row = curr_graph.get_row(row_id).unwrap();
                let mut neighbors = row.col_indices().to_owned();
                neighbors.sort_by(|a, b| {
                    row.get_entry(*a)
                        .unwrap()
                        .into_value()
                        .total_cmp(&row.get_entry(*b).unwrap().into_value())
                });
                for n in neighbors {
                    if let None = maybe_labels[n] {
                        maybe_labels[n] = Some(new_label);
                        break;
                    }
                }
            }
        }
        let labels: Vec<usize> = maybe_labels.iter().map(|x| x.unwrap()).collect();
        let (coarsed_graph, new_vertex_weights) = generate_new_matrix_from_labels(
            &curr_graph,
            &curr_vertex_weights,
            &labels,
            label_counter,
        );
        coarsen_steps.push(CoarsenData {
            graph: curr_graph,
            vertex_weights: curr_vertex_weights,
            labels,
        });
        curr_graph = coarsed_graph;
        curr_vertex_weights = new_vertex_weights;
    }
    coarsen_steps.push(CoarsenData {
        graph: curr_graph,
        vertex_weights: curr_vertex_weights,
        labels: Vec::new(),
    });
    coarsen_steps.reverse();
    debug_println!(
        "[COARSEN] -> heavy edge match: coarsen steps {}",
        coarsen_steps.len()
    );
    coarsen_steps
}

pub fn coarsen_2hop_heavy_edge_matching(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_size: usize,
    max_iterations: usize,
) -> Vec<CoarsenData> {
    let mut thread_rng = rand::thread_rng();
    let mut coarsen_steps: Vec<CoarsenData> = Vec::new();

    let mut curr_graph = graph.clone();
    let mut curr_vertex_weights = vertex_weights.clone();
    for _ in 0..max_iterations {
        let stop_2hop_at = ((curr_graph.nrows() / 4) * 3) / 2; // stop after around 75% are labels -> labelcounter > 0,375 * nrows

        let rows = curr_graph.nrows();
        if curr_graph.nrows() < target_size {
            break;
        }

        let mut label_counter: usize = 0;
        let mut maybe_labels: Vec<Option<usize>> = vec![None; rows];

        let mut row_order = Vec::from_iter(0..rows);
        row_order.shuffle(&mut thread_rng);

        for row_id in row_order.iter() {
            // label all matchings
            if let None = maybe_labels[*row_id] {
                // check for unlabeld neighbors and add the new_label
                let row = curr_graph.get_row(*row_id).unwrap();
                let mut neighbors = row.col_indices().to_owned();
                neighbors.sort_by(|a, b| {
                    row.get_entry(*a)
                        .unwrap()
                        .into_value()
                        .total_cmp(&row.get_entry(*b).unwrap().into_value())
                });
                for n in neighbors {
                    if let None = maybe_labels[n] {
                        let new_label = label_counter;
                        label_counter += 1;
                        maybe_labels[*row_id] = Some(new_label);
                        maybe_labels[n] = Some(new_label);
                        break;
                    }
                }
            }
        }

        if label_counter < stop_2hop_at {
            println!("[COARSEN] -> 2hop: find leaves");
            // find all leaves
            let mut leaves_map: HashMap<usize, Vec<usize>> = HashMap::new();
            for row_id in row_order.iter() {
                if let None = maybe_labels[*row_id] {
                    let row = curr_graph.row(*row_id);
                    if row.nnz() == 1 {
                        let parent = row.col_indices()[0];
                        if leaves_map.contains_key(&parent) {
                            leaves_map.get_mut(&parent).unwrap().push(*row_id);
                        } else {
                            leaves_map.insert(parent, vec![*row_id]);
                        }
                    }
                }
            }
            // label all leaves if possible
            for mut leaves in leaves_map.into_values() {
                while leaves.len() >= 2 {
                    // debug_println!("Add two leaves");
                    let leave_1 = leaves.pop().unwrap();
                    let leave_2 = leaves.pop().unwrap();
                    maybe_labels[leave_1] = Some(label_counter);
                    maybe_labels[leave_2] = Some(label_counter);
                    label_counter += 1;
                }
            }
        }
        if label_counter < stop_2hop_at {
            // find twins
            println!("[COARSEN] -> 2hop: find twins");
            let unmatched: Vec<usize> = maybe_labels
                .iter()
                .enumerate()
                .filter(|(_, o)| o.is_none())
                .map(|(i, _)| i)
                .collect();
            let mut unmatched_grouped_by_degree: Vec<Vec<usize>> =
                Vec::from_iter((0..64).map(|_| Vec::new()));
            for row_id in unmatched {
                let row = curr_graph.row(row_id);
                if row.col_indices().len() > 1 && row.col_indices().len() < 64 {
                    unmatched_grouped_by_degree[row.col_indices().len()].push(row_id);
                }
            }
            for group in unmatched_grouped_by_degree {
                // copy idea from leaves, if twin, then they have the same parent with smallest index
                let mut sub_groups: HashMap<usize, Vec<usize>> = HashMap::new();
                for row_i_index in group {
                    let row = curr_graph.row(row_i_index);
                    let min_neighbor = row.col_indices().iter().min().unwrap();
                    if sub_groups.contains_key(min_neighbor) {
                        sub_groups.get_mut(min_neighbor).unwrap().push(row_i_index);
                    } else {
                        sub_groups.insert(*min_neighbor, vec![row_i_index]);
                    }
                }
                for sg in sub_groups.values() {
                    for row_i_index in sg.iter() {
                        if maybe_labels[*row_i_index].is_some() {
                            continue;
                        }
                        for row_j_index in sg.iter() {
                            if row_i_index == row_j_index || maybe_labels[*row_j_index].is_some() {
                                continue;
                            }
                            let row_i = curr_graph.row(*row_i_index);
                            let row_i_cols = row_i.col_indices();
                            let row_j = curr_graph.row(*row_j_index);
                            let row_j_cols = row_j.col_indices();
                            if row_i_cols == row_j_cols {
                                maybe_labels[*row_i_index] = Some(label_counter);
                                maybe_labels[*row_j_index] = Some(label_counter);
                                label_counter += 1;
                                break;
                            }
                        }
                    }
                }
            }
        }
        // find relatives
        if label_counter < stop_2hop_at {
            println!("[COARSEN] -> 2hop: find relatives");
            // find all leaves
            let mut relatives_map: HashMap<usize, Vec<usize>> = HashMap::new();
            for row_id in row_order.iter() {
                if let None = maybe_labels[*row_id] {
                    let row = curr_graph.row(*row_id);
                    if row.nnz() == 1 {
                        let parents = row.col_indices();
                        for p in parents {
                            if relatives_map.contains_key(&p) {
                                relatives_map.get_mut(&p).unwrap().push(*row_id);
                            } else {
                                relatives_map.insert(*p, vec![*row_id]);
                            }
                        }
                    }
                }
            }
            // label all relatives if possible
            for mut leaves in relatives_map.into_values() {
                while leaves.len() >= 2 {
                    // debug_println!("Add two leaves");
                    let leave_1 = leaves.pop().unwrap();
                    let leave_2 = leaves.pop().unwrap();
                    maybe_labels[leave_1] = Some(label_counter);
                    maybe_labels[leave_2] = Some(label_counter);
                    label_counter += 1;
                }
            }
        }

        // fill unmatched
        for row_id in row_order.iter() {
            if let None = maybe_labels[*row_id] {
                maybe_labels[*row_id] = Some(label_counter);
                label_counter += 1;
            }
        }

        let labels: Vec<usize> = maybe_labels.iter().map(|x| x.unwrap()).collect();
        let (coarsed_graph, new_vertex_weights) = generate_new_matrix_from_labels(
            &curr_graph,
            &curr_vertex_weights,
            &labels,
            label_counter,
        );
        coarsen_steps.push(CoarsenData {
            graph: curr_graph,
            vertex_weights: curr_vertex_weights,
            labels,
        });
        curr_graph = coarsed_graph;
        curr_vertex_weights = new_vertex_weights;
    }
    coarsen_steps.push(CoarsenData {
        graph: curr_graph,
        vertex_weights: curr_vertex_weights,
        labels: Vec::new(),
    });
    coarsen_steps.reverse();
    debug_println!(
        "[COARSEN] -> heavy edge match 2hop: coarsen steps {}",
        coarsen_steps.len()
    );
    coarsen_steps
}

pub fn generate_new_matrix_from_labels(
    org_matrix: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    labels: &Vec<usize>,
    new_size: usize,
) -> (CsrMatrix<f64>, Vec<f64>) {
    let mut construction_matrix: CooMatrix<f64> = CooMatrix::new(new_size, new_size);
    let mut new_vertex_weights = vec![0.0; new_size];
    let old_size = org_matrix.nrows();
    for row in 0..old_size {
        let label_a = labels[row];
        new_vertex_weights[label_a] += vertex_weights[row];
        for edge in org_matrix.get_row(row).unwrap().col_indices() {
            let label_b = labels[*edge];
            if label_a != label_b {
                construction_matrix.push(
                    label_a,
                    label_b,
                    org_matrix.get_entry(row, *edge).unwrap().into_value(),
                );
            }
        }
    }
    let tmp = CsrMatrix::from(&construction_matrix);
    // println!("diag {}", tmp.diagonal_as_csr().nnz());
    // let triple: Vec<(usize, usize, f64)> = tmp.triplet_iter().map(|(i, j, v)| (i, j, *v)).collect();
    // println!("{:?}", triple);
    return (tmp, new_vertex_weights);
}
