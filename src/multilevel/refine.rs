use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum RefineAlgorithm {
    Interpolate,
    KL { max_iterations: usize },
    BKL { max_iterations: usize },
    FM,
}

pub fn refine_interpolate(labels: &Vec<usize>, coarse_partition: &Vec<u32>) -> Vec<u32> {
    let mut new_partition: Vec<u32> = vec![0; labels.len()];

    for (index, label) in labels.iter().enumerate() {
        new_partition[index] = coarse_partition[*label];
    }

    new_partition
}
