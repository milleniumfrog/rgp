use metis_sys::{partition_metis_kway, partition_metis_rb};
use nalgebra_sparse::CsrMatrix;
use rgp::{
    geometric::partition_recursive_coordinate_bisection,
    growing::{
        partition_graph_growing, partition_greedy_graph_growing, GraphGrowingBisectionAlgorithm,
    },
    helpers::{get_edgecut_weight, get_laplace_matrix, get_weighted_inbalance},
    import::{
        get_matrix_without_diag, import_coordinates_from_file, import_matrix_from_file,
        make_matrix_symmetric,
    },
    multilevel::{
        coarsen::CoarsenAlgorithm, partition_multilevel_graph_growing_rb,
        partition_multilevel_spectral_rb, refine::RefineAlgorithm,
    },
    random::partition_random,
    spectral::{
        partition_msb_coarsen_rb, partition_msb_rb, partition_spectral_rb, partition_spectral_rcb,
    },
    stream::partition_ldg,
};
use std::time::{Duration, UNIX_EPOCH};
use sysinfo::{Pid, ProcessExt, System, SystemExt};
use test::{read_results, Algorithm, GraphInput};
use zoltan2_sys::{partition_sphynx, SphynxPreconditioner};

use crate::test::{print_test_results, write_distribution_file, write_result, ResultCsvItem};

pub mod test;

fn main() {
    // PARAMETERS
    let graphs = vec![
        // imdb datasets
        GraphInput {
            name: "imdb_10_000".to_owned(),
            graph: "./test_files/10000.mtx".to_owned(),
            coordinates: None,
            mapping: Some("./test_files/mapping_10000.txt".to_owned()),
        },
        GraphInput {
            name: "imdb_100_000".to_owned(),
            graph: "./test_files/100000.mtx".to_owned(),
            coordinates: None,
            mapping: Some("./test_files/mapping_100000.txt".to_owned()),
        },
        GraphInput {
            name: "imdb_1_000_000".to_owned(),
            graph: "./test_files/1000000.mtx".to_owned(),
            coordinates: None,
            mapping: Some("./test_files/mapping_1000000.txt".to_owned()),
        },
        GraphInput {
            name: "imdb_10_000_000".to_owned(),
            graph: "./test_files/10000000.mtx".to_owned(),
            coordinates: None,
            mapping: Some("./test_files/mapping_10000000.txt".to_owned()),
        },
        GraphInput {
            name: "imdb_15_000_000".to_owned(),
            graph: "./test_files/15000000.mtx".to_owned(),
            coordinates: None,
            mapping: Some("./test_files/mapping_15000000.txt".to_owned()),
        },
        GraphInput {
            name: "imdb_20_000_000".to_owned(),
            graph: "./test_files/20000000.mtx".to_owned(),
            coordinates: None,
            mapping: Some("./test_files/mapping_20000000.txt".to_owned()),
        },
        // other datasets
        GraphInput {
            graph: "./test_files/bcspwr08.mtx".to_owned(),
            coordinates: None,
            mapping: None,
            name: "bcspwr08".to_owned(),
        },
        GraphInput {
            graph: "./test_files/barth5.mtx".to_owned(),
            coordinates: None,
            mapping: None,
            name: "barth5".to_owned(),
        },
        GraphInput {
            graph: "./test_files/bcsstk30.mtx".to_owned(),
            coordinates: None,
            mapping: None,
            name: "bcsstk30".to_owned(),
        },
        GraphInput {
            graph: "./test_files/luxembourg_osm.mtx".to_owned(),
            coordinates: Some("./test_files/luxembourg_osm_coord.mtx".to_owned()),
            mapping: None,
            name: "luxembourg_osm".to_owned(),
        },
        GraphInput {
            graph: "./test_files/belgium_osm.mtx".to_owned(),
            coordinates: Some("./test_files/belgium_osm_coord.mtx".to_owned()),
            mapping: None,
            name: "belgium_osm".to_owned(),
        },
        GraphInput {
            graph: "./test_files/europe_osm.mtx".to_owned(),
            coordinates: Some("./test_files/europe_osm_coord.mtx".to_owned()),
            mapping: None,
            name: "europe_osm".to_owned(),
        },
    ];

    let algorithms: Vec<Algorithm> = vec![
        Algorithm::Random,
        // Multilevel
        Algorithm::MetisRB,
        Algorithm::MetisKWAY,
        // Algorithm::MultilevelGraphGrowing(
        //     CoarsenAlgorithm::HeavyEdgeMatch {
        //         target_size: 1000,
        //         max_iterations: 100,
        //     },
        //     GraphGrowingBisectionAlgorithm::GGGP { iterations: 10 },
        //     RefineAlgorithm::FM,
        // ),
        Algorithm::MultilevelGraphGrowing(
            CoarsenAlgorithm::HeavyEdgeMatch2Hop {
                target_size: 1000,
                max_iterations: 100,
            },
            GraphGrowingBisectionAlgorithm::GGGP { iterations: 10 },
            RefineAlgorithm::FM,
        ),
        // graph growing
        Algorithm::GGP { iterations: 10 },
        Algorithm::GreedyGGP { iterations: 2 },
        // stream
        Algorithm::StreamLinearDeterminsticGreedy,
        // spectral algorithms
        Algorithm::RSB,
        Algorithm::SpectralRCB(None),
        Algorithm::SpectralRCB(Some(2)),
        Algorithm::MSB,
        // Algorithm::MSBCoarsen(CoarsenAlgorithm::FirstMatch {
        //     target_size: 1000,
        //     max_iterations: 500,
        // }),
        // Algorithm::MSBCoarsen(CoarsenAlgorithm::HeavyEdgeMatch {
        //     target_size: 1000,
        //     max_iterations: 500,
        // }),
        Algorithm::MSBCoarsen(CoarsenAlgorithm::HeavyEdgeMatch2Hop {
            target_size: 1000,
            max_iterations: 500,
        }),
        // Algorithm::MultilevelSpectral(
        //     CoarsenAlgorithm::HeavyEdgeMatch {
        //         target_size: 1000,
        //         max_iterations: 100,
        //     },
        //     RefineAlgorithm::FM,
        // ),
        Algorithm::MultilevelSpectral(
            CoarsenAlgorithm::HeavyEdgeMatch2Hop {
                target_size: 1000,
                max_iterations: 100,
            },
            RefineAlgorithm::FM,
        ),
        Algorithm::SPHYNX(SphynxPreconditioner::Jacobi),
        // Algorithm::SPHYNX(SphynxPreconditioner::Muelu),
    ];

    let target_partition_count_list: Vec<u32> = vec![2, 8, 16, 32, 64, 256];

    let timeout_in_seconds = 60 * 60 * 2; // 2 hours

    let run_process = true;

    let runs: usize = 2;

    let result_file_path: String = "./results/result.csv".to_owned();

    let max_memory_in_mb = 48_000;

    // MAIN
    procspawn::init();

    let executed_results = read_results(&result_file_path);
    for run in 0..runs {
        for target_partition_count in target_partition_count_list.clone() {
            for alg in algorithms.clone() {
                'graphloop: for graph in graphs.clone() {
                    for er in executed_results.iter() {
                        if er.algorithm == format!("{:?}", alg)
                            && er.matrix == graph.graph
                            && er.run == run
                            && er.partition_count == target_partition_count
                        {
                            println!("already executed {} {:?} {}", run, alg, graph.graph);
                            continue 'graphloop;
                        }
                    }

                    if run_process {
                        let process_arg = graph.clone();
                        let mut handler = procspawn::spawn(
                            (process_arg, alg.clone(), target_partition_count),
                            run_algorithm,
                        );
                        let pid = handler.pid().unwrap() as usize;
                        let start = std::time::SystemTime::now()
                            .duration_since(UNIX_EPOCH)
                            .unwrap()
                            .as_secs();
                        let expected_end_time = start + timeout_in_seconds;
                        let mut finished_in_time = false;
                        let mut out_of_memory = false;
                        let mut sys = System::new_all();
                        let mut max_memory: u64 = 0;
                        while std::time::SystemTime::now()
                            .duration_since(UNIX_EPOCH)
                            .unwrap()
                            .as_secs()
                            < expected_end_time
                        {
                            // check memory consumtion
                            {
                                sys.refresh_all();
                                let process = sys.process(Pid::from(pid));
                                if let Some(process) = process {
                                    let memory = process.memory();
                                    let memory = memory / 1000000;
                                    max_memory = u64::max(max_memory, memory);
                                    println!("memory: {}MB", memory);
                                    if memory > max_memory_in_mb {
                                        println!("Kill process out of memory");
                                        out_of_memory = true;
                                        let result = ResultCsvItem {
                                            algorithm: format!("{:?}", alg),
                                            edge_cut: None,
                                            edge_cut_promille: None,
                                            inbalance: None,
                                            matrix: graph.graph.clone(),
                                            timeout: false,
                                            out_of_memory: true,
                                            time: None,
                                            run,
                                            max_memory,
                                            partition_count: target_partition_count,
                                        };
                                        write_result(&result_file_path, &result);
                                        handler.kill().unwrap();
                                        break;
                                    }
                                }
                            }
                            // check process state
                            match handler.join_timeout(Duration::from_micros(100)) {
                                Ok((partition, edge_cut, edge_cut_promille, inbalance)) => {
                                    println!("Finished process {} for graph {}", pid, graph.graph);
                                    finished_in_time = true;
                                    let result = ResultCsvItem {
                                        algorithm: format!("{:?}", alg),
                                        edge_cut: Some(edge_cut),
                                        edge_cut_promille: Some(edge_cut_promille),
                                        inbalance: Some(inbalance),
                                        matrix: graph.graph.clone(),
                                        timeout: false,
                                        out_of_memory: false,
                                        time: Some(
                                            (std::time::SystemTime::now()
                                                .duration_since(UNIX_EPOCH)
                                                .unwrap()
                                                .as_secs()
                                                - start)
                                                as usize,
                                        ),
                                        run,
                                        max_memory: max_memory,
                                        partition_count: target_partition_count,
                                    };
                                    write_result(&result_file_path, &result);

                                    if let Some(mapping) = graph.mapping {
                                        if run == 0 {
                                            write_distribution_file(
                                                &graph.name,
                                                &mapping,
                                                &partition,
                                                alg.clone(),
                                                target_partition_count,
                                            );
                                        }
                                    }

                                    break;
                                }
                                Err(e) => {
                                    if e.is_timeout() {
                                        std::thread::sleep(Duration::from_secs(1));
                                        continue;
                                    }
                                    if e.is_cancellation() {
                                        println!("Killed process for graph {}", graph.graph);
                                        finished_in_time = true;
                                        break;
                                    } else if e.is_panic() {
                                        println!("The process paniced for graph {}", graph.graph);
                                        finished_in_time = true;
                                        break;
                                    } else if e.is_remote_close() {
                                        println!("Remote closed for graph {}", graph.graph);
                                        finished_in_time = true;
                                        break;
                                    } else {
                                        println!("Unknown error for graph {}", graph.graph);
                                        break;
                                    }
                                }
                            }
                        }
                        // handle if process did not finish in time
                        if !finished_in_time && !out_of_memory {
                            println!("Kill process");
                            let result = ResultCsvItem {
                                algorithm: format!("{:?}", alg),
                                edge_cut: None,
                                edge_cut_promille: None,
                                inbalance: None,
                                matrix: graph.graph.clone(),
                                timeout: true,
                                out_of_memory: false,
                                time: None,
                                run,
                                max_memory,
                                partition_count: target_partition_count,
                            };
                            write_result(&result_file_path, &result);
                            handler.kill().unwrap();
                        }
                    } else {
                        let args = (graph, alg.clone(), target_partition_count);
                        let handler = std::thread::spawn(|| run_algorithm(args));
                        handler.join().unwrap();
                    }
                }
            }
        }
    }
}

fn run_algorithm(
    (graphInput, alg, target_partition_count): (GraphInput, Algorithm, u32),
) -> (Vec<u32>, f64, usize, f64) {
    println!(
        "ARGS for process: \n\tGraph:{}\n\tCoordinates: {}\n\tAlgorithm: {:?}\npartitions: {}",
        graphInput.graph,
        graphInput.coordinates.clone().unwrap_or("--".to_owned()),
        alg,
        target_partition_count
    );
    let graph = import_matrix_from_file(&graphInput.graph).unwrap();
    println!("Imported graph");
    let graph = make_matrix_symmetric(&graph);
    println!("made graph undirected");
    let graph = get_matrix_without_diag(&graph);
    println!("removed loops");

    let default_vertex_weights = vec![1.0; graph.nrows()];
    let total_edge_weight: f64 = graph.values().iter().sum::<f64>() / 2.0;

    let partition = match alg.clone() {
        Algorithm::RCB => {
            let coordinates = match graphInput.coordinates {
                Some(path) => Some(import_coordinates_from_file(&path).unwrap()),
                None => None,
            };
            println!("Maybe imported coordinates");
            if let Some(coordinates) = coordinates {
                let partition = partition_recursive_coordinate_bisection(
                    &coordinates,
                    &default_vertex_weights,
                    target_partition_count,
                )
                .unwrap();
                print_test_results(
                    format!("{:?}", alg),
                    &graph,
                    &partition,
                    &default_vertex_weights,
                    total_edge_weight,
                    target_partition_count,
                );
                partition
            } else {
                println!("No coordinates provided");
                vec![0; graph.nrows()]
            }
        }
        Algorithm::RSB => {
            let partition =
                partition_spectral_rb(&graph, &default_vertex_weights, target_partition_count)
                    .unwrap();
            print_test_results(
                format!("{:?}", alg),
                &graph,
                &partition,
                &default_vertex_weights,
                total_edge_weight,
                target_partition_count,
            );
            partition
        }
        Algorithm::MSB => {
            let partition =
                partition_msb_rb(&graph, &default_vertex_weights, target_partition_count).unwrap();
            print_test_results(
                format!("{:?}", alg),
                &graph,
                &partition,
                &default_vertex_weights,
                total_edge_weight,
                target_partition_count,
            );
            partition
        }
        Algorithm::MSBCoarsen(ca) => {
            let partition = partition_msb_coarsen_rb(
                &ca,
                &graph,
                &default_vertex_weights,
                target_partition_count,
            )
            .unwrap();
            print_test_results(
                format!("{:?}", alg),
                &graph,
                &partition,
                &default_vertex_weights,
                total_edge_weight,
                target_partition_count,
            );
            partition
        }
        Algorithm::SPHYNX(precond) => {
            let laplace = get_laplace_matrix(&graph);
            let partition =
                partition_sphynx(&laplace, target_partition_count, precond.clone()).unwrap();
            print_test_results(
                format!("{:?}", alg),
                &graph,
                &partition,
                &default_vertex_weights,
                total_edge_weight,
                target_partition_count,
            );
            partition
        }
        Algorithm::SpectralRCB(n_eigenvectors) => {
            let partition = partition_spectral_rcb(
                &graph,
                &default_vertex_weights,
                target_partition_count,
                n_eigenvectors,
            )
            .unwrap();
            print_test_results(
                format!("{:?}", alg),
                &graph,
                &partition,
                &default_vertex_weights,
                total_edge_weight,
                target_partition_count,
            );
            partition
        }
        Algorithm::Random => {
            let partition = partition_random(graph.nrows(), target_partition_count);
            print_test_results(
                format!("{:?}", alg),
                &graph,
                &partition,
                &default_vertex_weights,
                total_edge_weight,
                target_partition_count,
            );
            partition
        }
        Algorithm::StreamLinearDeterminsticGreedy => {
            let partition = partition_ldg(&graph, &default_vertex_weights, target_partition_count);
            print_test_results(
                format!("{:?}", alg),
                &graph,
                &partition,
                &default_vertex_weights,
                total_edge_weight,
                target_partition_count,
            );
            partition
        }
        Algorithm::MetisRB => {
            let i32_graph = CsrMatrix::try_from_csr_data(
                graph.nrows(),
                graph.ncols(),
                graph.row_offsets().iter().cloned().collect::<Vec<usize>>(),
                graph.col_indices().iter().cloned().collect::<Vec<usize>>(),
                graph
                    .values()
                    .iter()
                    .map(|x| *x as i32)
                    .collect::<Vec<i32>>(),
            )
            .unwrap();
            let partition = partition_metis_rb(&i32_graph, target_partition_count);
            print_test_results(
                format!("{:?}", alg),
                &graph,
                &partition,
                &default_vertex_weights,
                total_edge_weight,
                target_partition_count,
            );
            partition
        }
        Algorithm::MetisKWAY => {
            let i32_graph = CsrMatrix::try_from_csr_data(
                graph.nrows(),
                graph.ncols(),
                graph.row_offsets().iter().cloned().collect::<Vec<usize>>(),
                graph.col_indices().iter().cloned().collect::<Vec<usize>>(),
                graph
                    .values()
                    .iter()
                    .map(|x| *x as i32)
                    .collect::<Vec<i32>>(),
            )
            .unwrap();
            let partition = partition_metis_kway(&i32_graph, target_partition_count);
            print_test_results(
                format!("{:?}", alg),
                &graph,
                &partition,
                &default_vertex_weights,
                total_edge_weight,
                target_partition_count,
            );
            partition
        }
        Algorithm::MultilevelSpectral(ca, ra) => {
            let partition = partition_multilevel_spectral_rb(
                &ca,
                &ra,
                &graph,
                &default_vertex_weights,
                target_partition_count,
            )
            .unwrap();
            print_test_results(
                format!("{:?}", alg),
                &graph,
                &partition,
                &default_vertex_weights,
                total_edge_weight,
                target_partition_count,
            );
            partition
        }
        Algorithm::MultilevelGraphGrowing(ca, ga, ra) => {
            let partition = partition_multilevel_graph_growing_rb(
                &ca,
                &ga,
                &ra,
                &graph,
                &default_vertex_weights,
                target_partition_count,
            )
            .unwrap();
            print_test_results(
                format!("{:?}", alg),
                &graph,
                &partition,
                &default_vertex_weights,
                total_edge_weight,
                target_partition_count,
            );
            partition
        }
        Algorithm::GGP { iterations } => {
            let partition = partition_graph_growing(
                &graph,
                &default_vertex_weights,
                target_partition_count,
                Some(iterations),
            )
            .unwrap();
            print_test_results(
                format!("{:?}", alg),
                &graph,
                &partition,
                &default_vertex_weights,
                total_edge_weight,
                target_partition_count,
            );
            partition
        }
        Algorithm::GreedyGGP { iterations } => {
            let partition = partition_greedy_graph_growing(
                &graph,
                &default_vertex_weights,
                target_partition_count,
                Some(iterations),
            )
            .unwrap();
            print_test_results(
                format!("{:?}", alg),
                &graph,
                &partition,
                &default_vertex_weights,
                total_edge_weight,
                target_partition_count,
            );
            partition
        }
    };

    let edge_cut = get_edgecut_weight(&graph, &partition);
    let edge_cut_promille = (edge_cut * 1000.0 / total_edge_weight) as usize;
    let inbalance =
        get_weighted_inbalance(&default_vertex_weights, &partition, target_partition_count);
    return (partition, edge_cut, edge_cut_promille, inbalance);
}
