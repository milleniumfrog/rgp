use std::ffi::{c_char, CString};

use arpack_ng_sys::{dsaupd_c, dseupd_c};
use nalgebra::{DMatrix, DVector};
use nalgebra_sparse::{CooMatrix, CsrMatrix};

use super::solver::symmlq;

/// Calculates the second eigenvalue for an laplace matrix and return
/// the algebraic connectivity and the fiedler vector.
///
/// This matrix does not check if the input matrix is an laplace matrix.
///
/// # Todo
/// add error handling
///
/// # Resources
/// based on
pub fn get_fiedler_vector(
    graph: &CsrMatrix<f64>,
) -> Result<(f64, DVector<f64>), GetEigenpairsLanczosError> {
    let eig_vecs = get_eigenpairs_lanczos(graph, 2)?;
    return Ok(eig_vecs[1].clone());
}
#[derive(Debug)]
pub enum GetEigenpairsLanczosError {
    DsaupdInfoNe0(i32),
    DseupdInfoNe0(i32),
}

/// Calculates the first {nev} eigenvalues for a matrix.
///
/// # Todo
/// add better error handling
pub fn get_eigenpairs_lanczos(
    graph: &CsrMatrix<f64>,
    nev: usize,
) -> Result<Vec<(f64, DVector<f64>)>, GetEigenpairsLanczosError> {
    // stores how big the matrix is
    let n = graph.nrows();
    // calculate the k lowest eigenvalues
    if nev >= n {
        panic!("nev must be smaller than n");
    }
    if nev <= 0 {
        panic!("nev must be bigger than 0");
    }
    let nev = nev as i32;
    let n = n as i32;
    // The number of Lanczos vectors generated. ``min(n, max(2*k + 1, 20))``
    let ncv: i32 = i32::min(n, i32::max(2 * nev + 1, 20));
    // max iterations
    let maxiter = n * 1000;
    // tolerance
    let tol = 0.0001;
    // mode
    let mode = 1;

    // let mut converged = false;

    let mut ido = 0;

    let mut resid: DVector<f64> = DVector::from_element(n as usize, 1.0);
    resid.fill(1.0);
    let mut v: DMatrix<f64> = DMatrix::zeros(n as usize, ncv as usize);
    let mut iparam: Vec<i32> = vec![0; 11];
    iparam[0] = 1;
    iparam[2] = maxiter;
    iparam[3] = 1;
    iparam[6] = mode;

    let mut workd = vec![0.0; 3 * n as usize];
    let lworkl = ncv * (ncv + 8);
    let mut workl = vec![0.0; lworkl as usize];
    let mut ipntr: Vec<i32> = vec![0; 11];
    let mut info = 0;

    let bmat = CString::new("I").unwrap();

    // calculate the ritz values
    for _ in 0..maxiter {
        unsafe {
            dsaupd_c(
                &mut ido,
                bmat.clone().into_raw(),
                n,
                "SM".as_ptr() as *const c_char,
                nev,
                tol,
                resid.as_mut_ptr(),
                ncv,
                v.as_mut_ptr() as *mut f64,
                n,
                iparam.as_mut_ptr(),
                ipntr.as_mut_ptr(),
                workd.as_mut_ptr(),
                workl.as_mut_ptr(),
                lworkl,
                &mut info,
            );

            let x_start_ptr = (ipntr[0] - 1) as usize;
            let y_start_ptr = (ipntr[1] - 1) as usize;
            let x_slice: &mut [f64] =
                workd.as_mut_slice()[x_start_ptr..(x_start_ptr + (n as usize))].as_mut();
            let x_vec = {
                let mut tmp = CooMatrix::new(n as usize, 1);
                for (i, v) in x_slice.iter().enumerate() {
                    tmp.push(i, 0, *v);
                }
                CsrMatrix::from(&tmp)
            };
            if !(ido == -1 || ido == 1 || ido == 2 || ido == 3 || ido == 5) {
                break;
            }
            let y_vec: Result<CsrMatrix<f64>, String> = if ido == -1 || ido == 1 {
                // println!("[EV] --- iteration {}", i);
                Ok(graph * &x_vec)
            } else if ido == 2 {
                Ok(x_vec.clone())
            } else {
                Err("ARPACK requested user shifts.".to_owned())
            };
            let y_vec = y_vec.unwrap();
            let y_slice: &mut [f64] =
                workd.as_mut_slice()[y_start_ptr..(y_start_ptr + (n as usize))].as_mut();
            for (i, v) in y_vec.values().iter().enumerate() {
                y_slice[i] = *v;
            }
        }
    }
    if info != 0 {
        return Err(GetEigenpairsLanczosError::DsaupdInfoNe0(info));
    }
    //
    let rvec = 1;
    let howmny = "A".as_ptr() as *const c_char;
    let mut select: DVector<i32> = DVector::zeros(ncv as usize);
    let mut vec_d: DVector<f64> = DVector::zeros(ncv as usize);
    let mut mat_z: DMatrix<f64> = DMatrix::zeros(n as usize, nev as usize);
    let ldz = n;
    // extract eigenvalues from the ritz values
    unsafe {
        dseupd_c(
            rvec,
            howmny,
            select.as_mut_ptr(),
            vec_d.as_mut_ptr(),
            mat_z.as_mut_ptr(),
            ldz,
            0.0,
            bmat.into_raw(),
            n,
            "SM".as_ptr() as *const c_char,
            nev,
            tol,
            resid.as_mut_ptr(),
            ncv,
            v.as_mut_ptr(),
            n,
            iparam.as_mut_ptr(),
            ipntr.as_mut_ptr(),
            workd.as_mut_ptr(),
            workl.as_mut_ptr(),
            lworkl,
            &mut info,
        )
    }
    if info != 0 {
        return Err(GetEigenpairsLanczosError::DseupdInfoNe0(info));
    }
    let k_ok = iparam[4] as usize;
    let eigen_values: DVector<f64> = DVector::from_column_slice(&vec_d.as_slice()[0..k_ok]);
    let mut eigen_vectors: DMatrix<f64> = DMatrix::zeros(graph.nrows(), nev as usize);
    for i in 0..k_ok as usize {
        eigen_vectors.set_column(i, &mat_z.column(i));
    }
    let mut result = Vec::new();
    for i in 0..nev as usize {
        result.push((eigen_values[i], DVector::from(eigen_vectors.column(i))))
    }
    return Ok(result);
}

/// Rayleigh quotient iteration with symmlq
pub fn rqi(matrix: &CsrMatrix<f64>, v: &DVector<f64>, tol: f64) -> DVector<f64> {
    let mut v: DVector<f64> = v.clone() / v.norm();
    let mut theta: f64 = (CsrMatrix::from(&v.transpose()) * matrix * &v)[0];
    let mut rho = f64::MAX;
    while rho > tol {
        let m = matrix - (theta * CsrMatrix::identity(matrix.nrows()));
        let tmp_v = &v / v.norm();
        let x = symmlq(&m, &tmp_v, tol);
        v = &x / x.norm();
        theta = (CsrMatrix::from(&v.transpose()) * matrix * &v)[0];
        let l_v: DVector<f64> = matrix * &v;
        rho = f64::sqrt(l_v.dot(&l_v) - (theta * theta));
        // println!("\t\t\t rqi rho {}", rho);
    }
    return v;
}
