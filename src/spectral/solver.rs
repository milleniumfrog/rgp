use nalgebra::DVector;
use nalgebra_sparse::CsrMatrix;

// /// computes (A - mu * I) * x = b
pub fn symmlq(matrix_a: &CsrMatrix<f64>, b: &DVector<f64>, tol: f64) -> DVector<f64> {
    let n = matrix_a.nrows();

    let mut x: DVector<f64> = DVector::zeros(n);
    let r: DVector<f64> = b.clone();
    let rho = r.norm();
    let mut v: DVector<f64> = r / rho;
    let mut beta = 0.0;
    let mut beta_tilde = 0.0;
    let mut c = -1.0;
    let mut s = 0.0;
    let mut kappa = rho;
    let mut v_old: DVector<f64> = DVector::zeros(n);
    let mut w: DVector<f64> = v.clone();
    let mut g = 0.0;
    let mut g_tilde_tilde = rho;

    let mut iteration_counter = 0;

    while kappa > tol && iteration_counter < 3 * n {
        iteration_counter += 1;
        // lanczos
        let v_hat: DVector<f64> = (matrix_a * &v) - (beta * &v_old);
        let alpha = v.dot(&v_hat);
        let v_hat: DVector<f64> = v_hat - (alpha * &v);
        beta = v_hat.norm();
        v_old = v.clone();
        v = v_hat / beta;
        // qr decomposition of lanczos
        let l_1 = (s * alpha) - c * beta_tilde;
        let l_2 = s * beta;
        let alpha_tilde = -(s * beta_tilde) - (c * alpha);
        beta_tilde = c * beta;
        let l_0 = f64::sqrt(f64::powf(alpha_tilde, 2.0) + f64::powf(beta, 2.0));
        c = alpha_tilde / l_0;
        s = beta / l_0;
        //
        let g_tilde = g_tilde_tilde - (l_1 * g);
        g_tilde_tilde = -l_2 * g;
        g = g_tilde / l_0;
        // approx solution
        x = x + (g * c * &w) + (g * s * &v);
        // search vector
        w = (s * &w) - (c * &v);
        // update residual norm
        kappa = f64::sqrt(f64::powf(g_tilde, 2.0) + f64::powf(g_tilde_tilde, 2.0));
        // println!("{} :: {} ", iteration_counter, kappa);
    }
    return x;
}
