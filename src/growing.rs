use std::collections::HashMap;

use debug_print::debug_println;
use nalgebra_sparse::CsrMatrix;

use crate::{
    apply_component_partition, extract_components_for_partition,
    helpers::{get_edgecut_weight, get_sub_graph},
};

#[derive(Debug, serde::Serialize, serde::Deserialize, Clone, PartialEq)]
pub enum GraphGrowingBisectionAlgorithm {
    GGP { iterations: usize },
    GGGP { iterations: usize },
}

#[derive(Debug)]
pub enum GraphGrowingError {
    TargetPartitionCountNotPowerOfTwo,
    GraphGrowingBisectionError(GraphGrowingBisectionError),
}

impl From<GraphGrowingBisectionError> for GraphGrowingError {
    fn from(value: GraphGrowingBisectionError) -> Self {
        return GraphGrowingError::GraphGrowingBisectionError(value);
    }
}

#[derive(Debug)]
pub enum GraphGrowingBisectionError {
    InvalidTargetWeight,
    IterationsEqualZero,
}

pub fn partition_graph_growing(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_partition_count: u32,
    iterations: Option<usize>,
) -> Result<Vec<u32>, GraphGrowingError> {
    if !target_partition_count.is_power_of_two() {
        return Err(GraphGrowingError::TargetPartitionCountNotPowerOfTwo);
    }

    let iterations = iterations.unwrap_or(4);

    let target_depth = u32::ilog2(target_partition_count);

    let partition = internal_graph_growing(&graph, &vertex_weights, 0, target_depth, iterations)?;
    return Ok(partition);
}

fn internal_graph_growing(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    depth: u32,
    target_depth: u32,
    iterations: usize,
) -> Result<Vec<u32>, GraphGrowingError> {
    let initial_partition = graph_growing_bisection(&graph, &vertex_weights, iterations, None)?;
    let partition = if depth < target_depth - 1 {
        let (subgraph_a, vertex_weights_a) =
            get_sub_graph(&graph, &vertex_weights, &initial_partition, 0);
        let (subgraph_b, vertex_weights_b) =
            get_sub_graph(&graph, &vertex_weights, &initial_partition, 1);
        let partition_a = internal_graph_growing(
            &subgraph_a,
            &vertex_weights_a,
            depth + 1,
            target_depth,
            iterations,
        )?;
        let partition_b = internal_graph_growing(
            &subgraph_b,
            &vertex_weights_b,
            depth + 1,
            target_depth,
            iterations,
        )?;
        // combine the recursive partitions
        let mut tmp_partition: Vec<u32> = vec![0; initial_partition.len()];
        let mut partition_a_counter = 0;
        let mut partition_b_counter = 0;
        for i in 0..initial_partition.len() {
            if initial_partition[i] == 0 {
                tmp_partition[i] = partition_a[partition_a_counter];
                partition_a_counter += 1;
            } else {
                tmp_partition[i] = partition_b[partition_b_counter]
                    + ((2 as u32)
                        .pow(target_depth.abs_diff(depth).abs_diff(1).try_into().unwrap()));
                partition_b_counter += 1;
            }
        }
        tmp_partition
    } else {
        initial_partition
    };
    return Ok(partition);
}

pub fn graph_growing_bisection(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    iterations: usize,
    target_weight: Option<f64>,
) -> Result<Vec<u32>, GraphGrowingBisectionError> {
    let total_weight: f64 = vertex_weights.iter().sum();
    let target_weight = target_weight.unwrap_or(total_weight / 2.0);
    if target_weight < 0.0 || target_weight > total_weight {
        return Err(GraphGrowingBisectionError::InvalidTargetWeight);
    }

    let graph_size = graph.nrows();
    let components_for_bipartition =
        extract_components_for_partition(&graph, &vertex_weights, target_weight);
    let component_to_partition = &components_for_bipartition.component_to_partition;
    let new_target_weight = components_for_bipartition.new_target_weight;
    let mut component_partition: Option<Vec<u32>> = None;
    for i in 0..iterations {
        let random_start_vertex = rand::random::<usize>() % component_to_partition.graph.nrows();
        debug_println!(
            "[GROWING] BFS based iteration {} with start vertex {}",
            i,
            random_start_vertex
        );
        let mut visited_nodes = vec![0; component_to_partition.graph.nrows()];
        let mut current_weight = 0.0;
        let mut next_visit_buffer = vec![random_start_vertex];
        while !next_visit_buffer.is_empty() && current_weight < new_target_weight {
            let tmp_visit_buffer = next_visit_buffer;
            next_visit_buffer = vec![];
            for vb in tmp_visit_buffer {
                if visited_nodes[vb] == 0 {
                    visited_nodes[vb] = 1;
                    current_weight += component_to_partition.vertex_weights[vb];
                    let row = component_to_partition.graph.row(vb);
                    let cols = row.col_indices();
                    for n in cols {
                        next_visit_buffer.push(*n);
                    }
                }
                if current_weight >= new_target_weight {
                    break;
                }
            }
        }
        if let Some(curr_component_partition) = component_partition {
            let old_edge_weight =
                get_edgecut_weight(&component_to_partition.graph, &curr_component_partition);
            let new_edge_weight = get_edgecut_weight(&component_to_partition.graph, &visited_nodes);
            component_partition = Some(if new_edge_weight > old_edge_weight {
                visited_nodes
            } else {
                curr_component_partition
            });
        } else {
            component_partition = Some(visited_nodes);
        }
    }

    if let Some(component_partition) = component_partition {
        let partition = apply_component_partition(
            graph_size,
            components_for_bipartition.components_in_subsection,
            &component_to_partition.vertex_ids,
            &component_partition,
        );
        return Ok(partition);
    } else {
        return Err(GraphGrowingBisectionError::IterationsEqualZero);
    }
}

pub fn partition_greedy_graph_growing(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_partition_count: u32,
    iterations: Option<usize>,
) -> Result<Vec<u32>, GraphGrowingError> {
    if !target_partition_count.is_power_of_two() {
        return Err(GraphGrowingError::TargetPartitionCountNotPowerOfTwo);
    }

    let iterations = iterations.unwrap_or(4);

    let target_depth = u32::ilog2(target_partition_count);

    let partition =
        internal_greedy_graph_growing(&graph, &vertex_weights, 0, target_depth, iterations)?;
    return Ok(partition);
}

fn internal_greedy_graph_growing(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    depth: u32,
    target_depth: u32,
    iterations: usize,
) -> Result<Vec<u32>, GraphGrowingError> {
    let initial_partition =
        greedy_graph_growing_bisection(&graph, &vertex_weights, iterations, None)?;
    let partition = if depth < target_depth - 1 {
        let (subgraph_a, vertex_weights_a) =
            get_sub_graph(&graph, &vertex_weights, &initial_partition, 0);
        let (subgraph_b, vertex_weights_b) =
            get_sub_graph(&graph, &vertex_weights, &initial_partition, 1);
        let partition_a = internal_greedy_graph_growing(
            &subgraph_a,
            &vertex_weights_a,
            depth + 1,
            target_depth,
            iterations,
        )?;
        let partition_b = internal_greedy_graph_growing(
            &subgraph_b,
            &vertex_weights_b,
            depth + 1,
            target_depth,
            iterations,
        )?;
        // combine the recursive partitions
        let mut tmp_partition: Vec<u32> = vec![0; initial_partition.len()];
        let mut partition_a_counter = 0;
        let mut partition_b_counter = 0;
        for i in 0..initial_partition.len() {
            if initial_partition[i] == 0 {
                tmp_partition[i] = partition_a[partition_a_counter];
                partition_a_counter += 1;
            } else {
                tmp_partition[i] = partition_b[partition_b_counter]
                    + ((2 as u32)
                        .pow(target_depth.abs_diff(depth).abs_diff(1).try_into().unwrap()));
                partition_b_counter += 1;
            }
        }
        tmp_partition
    } else {
        initial_partition
    };
    return Ok(partition);
}

pub fn greedy_graph_growing_bisection(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    iterations: usize,
    target_weight: Option<f64>,
) -> Result<Vec<u32>, GraphGrowingBisectionError> {
    let total_weight: f64 = vertex_weights.iter().sum();
    let target_weight = target_weight.unwrap_or(total_weight / 2.0);
    if target_weight < 0.0 || target_weight > total_weight {
        return Err(GraphGrowingBisectionError::InvalidTargetWeight);
    }

    let graph_size = graph.nrows();
    let components_for_bipartition =
        extract_components_for_partition(&graph, &vertex_weights, target_weight);
    let component_to_partition = &components_for_bipartition.component_to_partition;
    let new_target_weight = components_for_bipartition.new_target_weight;
    let mut component_partition: Option<Vec<u32>> = None;
    for i in 0..iterations {
        let random_start_vertex = rand::random::<usize>() % component_to_partition.graph.nrows();
        debug_println!(
            "[GROWING] Greedy BFS based iteration {} with start vertex {}",
            i,
            random_start_vertex
        );
        let mut boundary: HashMap<usize, Option<f64>> = HashMap::new();
        boundary.insert(random_start_vertex, None);
        let mut current_weight = 0.0;
        let mut current_partition = vec![0; component_to_partition.graph.nrows()];
        while boundary.len() > 0 && current_weight < new_target_weight {
            let mut current_min = f64::MAX;
            let mut current_index = 0;
            let mut new_gains: Vec<(usize, f64)> = Vec::new();
            for (v_index, v_gain) in boundary.iter() {
                let gain = if let Some(gain) = v_gain {
                    *gain
                } else {
                    let row: nalgebra_sparse::csr::CsrRow<'_, f64> =
                        component_to_partition.graph.row(*v_index);
                    let neighbors = row.col_indices();
                    let mut gain = 0.0;
                    for n in neighbors {
                        if current_partition[*n] == 1 {
                            gain -= component_to_partition
                                .graph
                                .get_entry(*v_index, *n)
                                .unwrap()
                                .into_value();
                        } else {
                            gain += component_to_partition
                                .graph
                                .get_entry(*v_index, *n)
                                .unwrap()
                                .into_value();
                        }
                    }
                    new_gains.push((*v_index, gain));
                    gain
                };
                if gain < current_min {
                    current_min = gain;
                    current_index = *v_index;
                }
            }
            for (ng_key, ng_gain) in new_gains {
                boundary.insert(ng_key, Some(ng_gain));
            }
            current_partition[current_index] = 1;
            boundary.remove(&current_index);
            current_weight += component_to_partition.vertex_weights[current_index];
            let row = component_to_partition.graph.row(current_index);
            let neighbors = row.col_indices();
            for n in neighbors {
                if current_partition[*n] == 0 {
                    boundary.insert(*n, None);
                }
            }
        }

        if let Some(curr_component_partition) = component_partition {
            let old_edge_weight =
                get_edgecut_weight(&component_to_partition.graph, &curr_component_partition);
            let new_edge_weight =
                get_edgecut_weight(&component_to_partition.graph, &current_partition);
            component_partition = Some(if new_edge_weight > old_edge_weight {
                current_partition
            } else {
                curr_component_partition
            });
        } else {
            component_partition = Some(current_partition);
        }
    }
    if let Some(component_partition) = component_partition {
        let partition = apply_component_partition(
            graph_size,
            components_for_bipartition.components_in_subsection,
            &component_to_partition.vertex_ids,
            &component_partition,
        );
        return Ok(partition);
    } else {
        return Err(GraphGrowingBisectionError::IterationsEqualZero);
    }
}
