use std::{cmp::Ordering, collections::HashSet};

use nalgebra_sparse::CsrMatrix;
use rand::seq::SliceRandom;

pub fn partition_ldg(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_partition_count: u32,
) -> Vec<u32> {
    let mut thread_rng = rand::thread_rng();

    let mut p_t: Vec<HashSet<usize>> = vec![HashSet::new(); target_partition_count as usize];
    let mut p_t_weight: Vec<f64> = vec![0.0; target_partition_count as usize];
    let total_weight = vertex_weights.iter().sum::<f64>();
    let constraint = total_weight / (target_partition_count as f64);

    let mut vertex_order = Vec::from_iter(0..graph.nrows());
    vertex_order.shuffle(&mut thread_rng);
    for vertex_index in vertex_order {
        let row = graph.get_row(vertex_index).unwrap();
        let cols = row.col_indices();
        let cols: HashSet<usize> = HashSet::from_iter(cols.into_iter().cloned());
        let mut weights: Vec<(usize, usize, f64)> = p_t
            .iter()
            .enumerate()
            .map(|(i, p)| {
                let weight = p_t_weight[i];
                (
                    i,
                    p.len(),
                    (p.intersection(&cols).count() as f64) * (1.0 - (weight / (constraint as f64))),
                )
            })
            .collect();
        // sort weights so the partition with the biggest weight is selected.
        // if the biggest weight is not unique use the partition with this weight
        // and the smallest partition size
        weights.sort_by(|(_, a_len, a_weight), (_, b_len, b_weight)| {
            if a_weight == b_weight {
                if a_len < b_len {
                    Ordering::Less
                } else if a_len == b_len {
                    Ordering::Equal
                } else {
                    Ordering::Greater
                }
            } else if a_weight < b_weight {
                Ordering::Greater
            } else {
                Ordering::Less
            }
        });
        let target_index = weights.first().unwrap().0;
        p_t.iter_mut().for_each(|p| {
            p.remove(&vertex_index);
        });
        p_t[target_index].insert(vertex_index);
        p_t_weight[target_index] += vertex_weights[vertex_index];
    }

    let mut partition = vec![0; graph.nrows()];
    for partition_index in 0..target_partition_count {
        for vertex_index in p_t[partition_index as usize].iter() {
            partition[*vertex_index] = partition_index;
        }
    }
    partition
}
