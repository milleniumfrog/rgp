use std::{
    fs::File,
    io::{self, BufRead},
    path::{self, Path},
};

use csv::{Reader, Writer, WriterBuilder};
use nalgebra_sparse::CsrMatrix;
use rgp::{
    growing::GraphGrowingBisectionAlgorithm,
    helpers::{get_edgecut_weight, get_weighted_inbalance},
    multilevel::{coarsen::CoarsenAlgorithm, refine::RefineAlgorithm},
};
use serde::{Deserialize, Serialize};
use std::io::Write;
use zoltan2_sys::SphynxPreconditioner;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GraphInput {
    pub graph: String,
    pub coordinates: Option<String>,
    pub mapping: Option<String>,
    pub name: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum Algorithm {
    RCB,
    RSB,
    MSB,
    MSBCoarsen(CoarsenAlgorithm),
    SPHYNX(SphynxPreconditioner),
    SpectralRCB(Option<u32>),
    Random,
    StreamLinearDeterminsticGreedy,
    MetisRB,
    MetisKWAY,
    MultilevelSpectral(CoarsenAlgorithm, RefineAlgorithm),
    MultilevelGraphGrowing(
        CoarsenAlgorithm,
        GraphGrowingBisectionAlgorithm,
        RefineAlgorithm,
    ),
    GGP {
        iterations: usize,
    },
    GreedyGGP {
        iterations: usize,
    },
}

pub fn print_test_results(
    name: String,
    graph: &CsrMatrix<f64>,
    partition: &Vec<u32>,
    vertex_weights: &Vec<f64>,
    total_edge_weight: f64,
    partition_count: u32,
) {
    let edgecut_weight = get_edgecut_weight(&graph, &partition);
    println!(
        "Results for {}:\n\tEdgecut weight: {}\n\tedges cut %: {}\n\tinbalance {}",
        name,
        edgecut_weight,
        ((edgecut_weight * 100.0 / total_edge_weight) as i32).to_string(),
        get_weighted_inbalance(&vertex_weights, &partition, partition_count)
    );
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ResultCsvItem {
    pub algorithm: String,
    pub matrix: String,
    pub timeout: bool,
    pub out_of_memory: bool,
    pub time: Option<usize>,
    pub max_memory: u64,
    pub inbalance: Option<f64>,
    pub edge_cut: Option<f64>,
    pub edge_cut_promille: Option<usize>,
    pub run: usize,
    pub partition_count: u32,
    pub error: Option<String>,
}

pub fn write_result(file_path: &String, res: &ResultCsvItem) {
    let mut writer = if path::Path::new(file_path).exists() {
        let io_writer = std::fs::File::options()
            .append(true)
            .open(file_path)
            .unwrap();
        WriterBuilder::new()
            .has_headers(false)
            .from_writer(io_writer)
    } else {
        Writer::from_path(file_path).unwrap()
    };
    writer.serialize(&res).unwrap();
}

pub fn read_results(file_path: &String) -> Vec<ResultCsvItem> {
    if path::Path::new(file_path).exists() {
        let reader = Reader::from_path(file_path).unwrap();
        reader
            .into_deserialize()
            .map(|csv_line| csv_line.unwrap())
            .collect()
    } else {
        Vec::new()
    }
}

pub fn write_distribution_file(
    graph_name: &String,
    mapping: &String,
    partition: &Vec<u32>,
    alg: Algorithm,
    partition_count: u32,
) {
    let dist_path = format!(
        "./results/dist/{}_{}_{}.dist",
        graph_name,
        format!("{:?}", alg).replace(" ", "_"),
        partition_count
    );
    if !Path::new(&dist_path).exists() {
        println!("Write distrition file");

        let mut dist_file = File::create(dist_path).unwrap();
        let mapping_file = File::open(mapping).unwrap();
        let mapping_buf_reader = io::BufReader::new(mapping_file).lines();
        let mapping = Vec::from_iter(mapping_buf_reader.into_iter().map(|line| line.unwrap()));
        for (i, p) in partition.iter().enumerate() {
            writeln!(dist_file, "{} {}", mapping[i], p).unwrap();
        }
    } else {
        println!("Dist file already exists");
    }
}
