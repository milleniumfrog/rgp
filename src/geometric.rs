use nalgebra::DMatrix;

#[derive(Debug)]
pub enum PartitionGeometricError {
    TargetCountNotPowerOfTwo,
    GeometricBisectionError(GeometricBisectionError),
}

impl From<GeometricBisectionError> for PartitionGeometricError {
    fn from(value: GeometricBisectionError) -> Self {
        return PartitionGeometricError::GeometricBisectionError(value);
    }
}

pub fn partition_recursive_coordinate_bisection(
    coordinates: &DMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_partition_count: u32,
) -> Result<Vec<u32>, PartitionGeometricError> {
    use PartitionGeometricError::*;

    if !target_partition_count.is_power_of_two() {
        return Err(TargetCountNotPowerOfTwo);
    }

    let target_depth = u32::ilog2(target_partition_count);

    let partition =
        internal_recursive_coordinate_bisection(&coordinates, vertex_weights, 0, target_depth)?;
    return Ok(partition);
}

#[derive(Debug)]
pub enum GeometricBisectionError {
    InvalidTargetWeight,
    NoColumns,
}

pub fn coordinate_bisection(
    coordinates: &DMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_weight: Option<f64>,
) -> Result<Vec<u32>, GeometricBisectionError> {
    let total_weight = vertex_weights.iter().sum::<f64>();
    let target_weight = target_weight.unwrap_or(total_weight / 2.0);
    if target_weight < 0.0 || target_weight > total_weight {
        return Err(GeometricBisectionError::InvalidTargetWeight);
    }

    let m = coordinates.ncols();

    if m == 0 {
        return Err(GeometricBisectionError::NoColumns);
    }

    let col_diff: Vec<f64> = coordinates
        .column_iter()
        .map(|column| column.max() - column.min())
        .collect();

    let max_col_diff_index = col_diff
        .iter()
        .cloned()
        .enumerate()
        .max_by(|(_, a), (_, b)| a.total_cmp(b))
        .unwrap()
        .0;

    let column = coordinates.column(max_col_diff_index);

    let seperator = {
        let mut curr_weight = 0.0;
        let mut vec = Vec::from_iter(column.iter().cloned().enumerate());
        vec.sort_by(|(_, a), (_, b)| match a.partial_cmp(b) {
            Some(o) => o,
            None => std::cmp::Ordering::Equal,
        });
        let mut curr_seperator = f64::MIN;
        for (i, v) in vec {
            if curr_weight < target_weight {
                curr_weight += vertex_weights[i];
                curr_seperator = v;
            } else {
                break;
            }
        }
        curr_seperator
    };

    let mut initial_partition: Vec<u32> = column
        .iter()
        .map(|x| if *x < seperator { 1 } else { 0 })
        .collect();
    let mut initial_partition_weight = {
        let mut weight: f64 = 0.0;
        for (i, v) in initial_partition.iter().enumerate() {
            if *v == 1 {
                weight += vertex_weights[i];
            }
        }
        weight
    };
    if initial_partition_weight < target_weight {
        for (i, v) in column.iter().enumerate() {
            if *v == seperator {
                initial_partition[i] = 1;
                initial_partition_weight += vertex_weights[i];
            }
            if initial_partition_weight >= target_weight {
                break;
            }
        }
    }

    return Ok(initial_partition);
}

fn internal_recursive_coordinate_bisection(
    coordinates: &DMatrix<f64>,
    vertex_weights: &Vec<f64>,
    depth: u32,
    target_depth: u32,
) -> Result<Vec<u32>, GeometricBisectionError> {
    let initial_partition = coordinate_bisection(&coordinates, &vertex_weights, None)?;

    let current_partition_size = initial_partition.iter().sum::<u32>() as usize;

    let partition = if depth < target_depth - 1 {
        let m = coordinates.ncols();
        // divide vertex_weights and coordinates
        let sub_coordinates_a: DMatrix<f64> = DMatrix::from_row_iterator(
            current_partition_size,
            m,
            coordinates
                .row_iter()
                .enumerate()
                .filter(|(row_index, _)| {
                    if initial_partition[*row_index] == 1 {
                        true
                    } else {
                        false
                    }
                })
                .map(|(_, row)| row.iter().map(|x| *x).collect::<Vec<f64>>())
                .flatten(),
        );
        let vertex_weights_a = vertex_weights
            .iter()
            .enumerate()
            .filter(|(i, _v)| initial_partition[*i] == 1)
            .map(|(_i, v)| *v)
            .collect();
        let sub_coordinates_b: DMatrix<f64> = DMatrix::from_row_iterator(
            initial_partition.len() - current_partition_size,
            m,
            coordinates
                .row_iter()
                .enumerate()
                .filter(|(row_index, _)| {
                    if initial_partition[*row_index] == 0 {
                        true
                    } else {
                        false
                    }
                })
                .map(|(_, row)| row.iter().map(|x| *x).collect::<Vec<f64>>())
                .flatten(),
        );
        let vertex_weights_b = vertex_weights
            .iter()
            .enumerate()
            .filter(|(i, _v)| initial_partition[*i] == 0)
            .map(|(_i, v)| *v)
            .collect();
        // calculate partition via recusion
        let partition_a = internal_recursive_coordinate_bisection(
            &sub_coordinates_a,
            &vertex_weights_a,
            depth + 1,
            target_depth,
        )?;
        let partition_b = internal_recursive_coordinate_bisection(
            &sub_coordinates_b,
            &vertex_weights_b,
            depth + 1,
            target_depth,
        )?;
        let mut tmp_partition: Vec<u32> = vec![0; initial_partition.len()];
        let mut partition_a_counter = 0;
        let mut partition_b_counter = 0;
        for i in 0..initial_partition.len() {
            if initial_partition[i] == 1 {
                tmp_partition[i] = partition_a[partition_a_counter];
                partition_a_counter += 1;
            } else {
                tmp_partition[i] = partition_b[partition_b_counter]
                    + ((2 as u32)
                        .pow(target_depth.abs_diff(depth).abs_diff(1).try_into().unwrap()));
                partition_b_counter += 1;
            }
        }
        tmp_partition
    } else {
        initial_partition
    };
    return Ok(partition);
}
