pub mod eigenvalues;
pub mod solver;

use std::collections::HashSet;

use nalgebra::{DMatrix, DVector};
use nalgebra_sparse::{CooMatrix, CsrMatrix};
use rand::{seq::SliceRandom, thread_rng};

use crate::{
    apply_component_partition, extract_components_for_partition,
    geometric::{partition_recursive_coordinate_bisection, PartitionGeometricError},
    growing::GraphGrowingBisectionError,
    helpers::{get_laplace_matrix, get_sub_graph},
    import::get_matrix_without_diag,
    multilevel::coarsen::{
        coarsen_2hop_heavy_edge_matching, coarsen_first_matching, coarsen_heavy_edge_matching,
        CoarsenAlgorithm,
    },
};

use self::eigenvalues::{
    get_eigenpairs_lanczos, get_fiedler_vector, rqi, GetEigenpairsLanczosError,
};

pub use zoltan2_sys::partition_sphynx;
pub use zoltan2_sys::SphynxPreconditioner;

#[derive(Debug)]
pub enum SpectralRbError {
    TargetPartitionCountNotPowerOfTwo,
    SpectralBisectionError(SpectralBisectionError),
}

impl From<SpectralBisectionError> for SpectralRbError {
    fn from(value: SpectralBisectionError) -> Self {
        return SpectralRbError::SpectralBisectionError(value);
    }
}

pub fn partition_spectral_rb(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_partition_count: u32,
) -> Result<Vec<u32>, SpectralRbError> {
    if !target_partition_count.is_power_of_two() {
        return Err(SpectralRbError::TargetPartitionCountNotPowerOfTwo);
    }

    let target_depth = u32::ilog2(target_partition_count);

    let partition = internal_spectral_rb(&graph, &vertex_weights, 0, target_depth)?;
    return Ok(partition);
}

pub fn internal_spectral_rb(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    depth: u32,
    target_depth: u32,
) -> Result<Vec<u32>, SpectralBisectionError> {
    let initial_partition = spectral_bisection(&graph, &vertex_weights, None)?;
    let partition = if depth < target_depth - 1 {
        let (subgraph_a, vertex_weights_a) =
            get_sub_graph(&graph, &vertex_weights, &initial_partition, 0);
        let (subgraph_b, vertex_weights_b) =
            get_sub_graph(&graph, &vertex_weights, &initial_partition, 1);
        let partition_a =
            internal_spectral_rb(&subgraph_a, &vertex_weights_a, depth + 1, target_depth)?;
        let partition_b =
            internal_spectral_rb(&subgraph_b, &vertex_weights_b, depth + 1, target_depth)?;
        // combine the recursive partitions
        let mut tmp_partition: Vec<u32> = vec![0; initial_partition.len()];
        let mut partition_a_counter = 0;
        let mut partition_b_counter = 0;
        for i in 0..initial_partition.len() {
            if initial_partition[i] == 0 {
                tmp_partition[i] = partition_a[partition_a_counter];
                partition_a_counter += 1;
            } else {
                tmp_partition[i] = partition_b[partition_b_counter]
                    + ((2 as u32)
                        .pow(target_depth.abs_diff(depth).abs_diff(1).try_into().unwrap()));
                partition_b_counter += 1;
            }
        }
        tmp_partition
    } else {
        initial_partition
    };
    return Ok(partition);
}

#[derive(Debug)]
pub enum SpectralBisectionError {
    /// target weight must be between 0 and the sum of vertex_weights
    InvalidTargetWeight,
    GetFiedlerVectorError(GetEigenpairsLanczosError),
    GraphGrowingBisectionError(GraphGrowingBisectionError),
    OrderingInvalid,
}

impl From<GetEigenpairsLanczosError> for SpectralBisectionError {
    fn from(value: GetEigenpairsLanczosError) -> Self {
        return SpectralBisectionError::GetFiedlerVectorError(value);
    }
}

impl From<GraphGrowingBisectionError> for SpectralBisectionError {
    fn from(value: GraphGrowingBisectionError) -> Self {
        return SpectralBisectionError::GraphGrowingBisectionError(value);
    }
}

pub fn spectral_bisection(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_weight: Option<f64>,
) -> Result<Vec<u32>, SpectralBisectionError> {
    let total_weight: f64 = vertex_weights.iter().sum();
    let target_weight = target_weight.unwrap_or(total_weight / 2.0);
    let graph_size = graph.nrows();
    if target_weight < 0.0 || target_weight > total_weight {
        return Err(SpectralBisectionError::InvalidTargetWeight);
    }

    let components_for_bipartition =
        extract_components_for_partition(&graph, &vertex_weights, target_weight);
    let component_to_partition = &components_for_bipartition.component_to_partition;
    let new_target_weight = components_for_bipartition.new_target_weight;

    if component_to_partition.graph.nrows() == 1 {
        let tmp_partition = apply_component_partition(
            graph_size,
            components_for_bipartition.components_in_subsection,
            &component_to_partition.vertex_ids,
            &vec![1],
        );
        return Ok(tmp_partition);
    }
    // ensure matrix is bigger than 30 rows
    let tmp_graph = if component_to_partition.graph.nrows() < 30 {
        let mut construction_matrix = CooMatrix::new(30, 30);
        for (x, y, v) in component_to_partition.graph.triplet_iter() {
            construction_matrix.push(x, y, *v);
        }
        CsrMatrix::from(&construction_matrix)
    } else {
        component_to_partition.graph.clone()
    };
    let laplace = get_laplace_matrix(&tmp_graph);
    let (_, fiedlervector): (f64, DVector<f64>) = get_fiedler_vector(&laplace)?;
    let fiedlervector: DVector<f64> = DVector::from_column_slice(
        &fiedlervector.as_slice()[0..component_to_partition.vertex_weights.len()],
    );

    // calculate a seperator
    let seperator = {
        let mut curr_weight = 0.0;
        let mut vec = Vec::from_iter(fiedlervector.iter().cloned().enumerate());
        vec.sort_by(|(_, a), (_, b)| match a.partial_cmp(b) {
            Some(o) => o,
            None => std::cmp::Ordering::Equal,
        });
        let mut curr_seperator = f64::MIN;
        for (i, v) in vec {
            if curr_weight < new_target_weight {
                curr_weight += component_to_partition.vertex_weights[i];
                curr_seperator = v;
            } else {
                break;
            }
        }
        curr_seperator
    };

    // calculate the initial partition
    let mut component_partition: Vec<u32> = fiedlervector
        .iter()
        .map(|x| if *x < seperator { 1 } else { 0 })
        .collect();
    let mut initial_partition_weight = {
        let mut weight = 0.0;
        for i in 0..component_partition.len() {
            if component_partition[i] == 1 {
                weight += component_to_partition.vertex_weights[i];
            }
        }
        weight
    };
    // move additional vertices until the target weight is reached
    if initial_partition_weight < new_target_weight {
        for (i, v) in fiedlervector.iter().enumerate() {
            if *v == seperator {
                component_partition[i] = 1;
                initial_partition_weight += vertex_weights[i];
            }
            if initial_partition_weight >= new_target_weight {
                break;
            }
        }
    }
    let tmp_partition = apply_component_partition(
        graph_size,
        components_for_bipartition.components_in_subsection,
        &components_for_bipartition.component_to_partition.vertex_ids,
        &component_partition,
    );

    return Ok(tmp_partition);
}

pub fn partition_msb_rb(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_partition_count: u32,
) -> Result<Vec<u32>, SpectralRbError> {
    if !target_partition_count.is_power_of_two() {
        return Err(SpectralRbError::TargetPartitionCountNotPowerOfTwo);
    }

    let target_depth = u32::ilog2(target_partition_count);

    let partition = internal_msb_rb(&graph, &vertex_weights, 0, target_depth)?;
    return Ok(partition);
}

fn internal_msb_rb(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    depth: u32,
    target_depth: u32,
) -> Result<Vec<u32>, SpectralBisectionError> {
    let initial_partition = msb_max_independend_set_bisection(&graph, &vertex_weights, None)?;
    let partition = if depth < target_depth - 1 {
        let (subgraph_a, vertex_weights_a) =
            get_sub_graph(&graph, &vertex_weights, &initial_partition, 0);
        let (subgraph_b, vertex_weights_b) =
            get_sub_graph(&graph, &vertex_weights, &initial_partition, 1);
        let partition_a = internal_msb_rb(&subgraph_a, &vertex_weights_a, depth + 1, target_depth)?;
        let partition_b = internal_msb_rb(&subgraph_b, &vertex_weights_b, depth + 1, target_depth)?;
        // combine the recursive partitions
        let mut tmp_partition: Vec<u32> = vec![0; initial_partition.len()];
        let mut partition_a_counter = 0;
        let mut partition_b_counter = 0;
        for i in 0..initial_partition.len() {
            if initial_partition[i] == 0 {
                tmp_partition[i] = partition_a[partition_a_counter];
                partition_a_counter += 1;
            } else {
                tmp_partition[i] = partition_b[partition_b_counter]
                    + ((2 as u32)
                        .pow(target_depth.abs_diff(depth).abs_diff(1).try_into().unwrap()));
                partition_b_counter += 1;
            }
        }
        tmp_partition
    } else {
        initial_partition
    };
    return Ok(partition);
}

pub fn msb_max_independend_set_bisection(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_weight: Option<f64>,
) -> Result<Vec<u32>, SpectralBisectionError> {
    let mut thread_rng = thread_rng();

    let total_weight: f64 = vertex_weights.iter().sum();
    let target_weight = target_weight.unwrap_or(total_weight / 2.0);
    let graph_size = graph.nrows();

    if target_weight < 0.0 || target_weight > total_weight {
        return Err(SpectralBisectionError::InvalidTargetWeight);
    }

    let components_for_bipartition =
        extract_components_for_partition(&graph, &vertex_weights, target_weight);
    let component_to_partition = &components_for_bipartition.component_to_partition;
    let new_target_weight = components_for_bipartition.new_target_weight;
    println!("Coarsen");
    let coarsed_data: Vec<(CsrMatrix<f64>, Vec<usize>)> = {
        let mut coarsed_data = Vec::new();
        let mut curr_graph = component_to_partition.graph.clone();
        for i in 0..100 {
            let n = curr_graph.nrows();
            println!("Coarsedn size {}", n);
            if n < 5000 {
                break;
            }
            let mut independend_set: Vec<usize> = Vec::new();
            let mut visited_nodes = vec![false; n];
            let mut rand_order = Vec::from_iter(0..n);
            rand_order.shuffle(&mut thread_rng);
            for row_ind in rand_order {
                if !visited_nodes[row_ind] {
                    // add node to independend set
                    visited_nodes[row_ind] = true;
                    independend_set.push(row_ind);
                    // mark all neighbors as already visited
                    let row = curr_graph.get_row(row_ind).unwrap();
                    for neighbor in row.col_indices() {
                        visited_nodes[*neighbor] = true;
                    }
                }
            }
            let mut added_edges: HashSet<(usize, usize)> = HashSet::new();
            let mut domain_list: Vec<HashSet<usize>> = independend_set
                .iter()
                .map(|x| HashSet::from([*x]))
                .collect();
            let mut node_multinode_storage: Vec<Vec<usize>> = vec![Vec::new(); n];
    
            for (i, domain) in domain_list.iter_mut().enumerate() {
                let old_domain = domain.clone();
                for node in old_domain {
                    let row: nalgebra_sparse::csr::CsrRow<'_, f64> = curr_graph.get_row(node).unwrap();
                    for n in row.col_indices() {
                        if !domain.contains(n) {
                            domain.insert(*n);
                            node_multinode_storage[*n].push(i);
                        }
                    }
                }
            }

            let domain_count = domain_list.len();
            let mut construction_matrix: CooMatrix<f64> = CooMatrix::new(domain_count, domain_count);
            for node_multinode in node_multinode_storage {
                if node_multinode.len() > 1 {
                    for i in node_multinode.iter() {
                        for j in node_multinode.iter() {
                            if *i == *j {
                                continue;
                            }
                            if !added_edges.contains(&(*i, *j)) {
                                added_edges.insert((*i, *j));
                                construction_matrix.push(*i, *j, 1.0);
                            }
                        }
                    }
                }
            }
            let labels = independend_set;
            let coarsed_graph = CsrMatrix::from(&construction_matrix);
            if coarsed_graph.nrows() > ((n * 90) / 100) {
                println!("stopped coarsening at {} iteration", i);
                break;
            }
            curr_graph = coarsed_graph.clone();
            coarsed_data.push((coarsed_graph, labels));
        }
        coarsed_data.reverse();
        coarsed_data
    };

    let mut coarsed_data_iterator = coarsed_data.iter();
    let default_values = (component_to_partition.graph.clone(), Vec::from_iter(0..component_to_partition.graph.nrows()));
    let (coarsed_graph, labels) = match coarsed_data_iterator.next() {
        Some(v) => v,
        None => &default_values
    };
    let mut labels = labels.clone();
    if coarsed_graph.nrows() == 1 {
        println!("Coarsed graph has size 1, so use spectral bisection instead to avoid problems");
        return spectral_bisection(&graph, &vertex_weights, Some(target_weight));
    }

    // ensure matrix is bigger than 30 entries
    let coarsed_graph = if coarsed_graph.nrows() < 30 {
        let mut construction_matrix = CooMatrix::new(30, 30);
        for (x, y, v) in coarsed_graph.triplet_iter() {
            construction_matrix.push(x, y, *v);
        }
        CsrMatrix::from(&construction_matrix)
    } else {
        coarsed_graph.clone()
    };

    println!(
        "Initial Partition graph with size: {}",
        coarsed_graph.nrows()
    );
    let coarsed_graph = get_laplace_matrix(&coarsed_graph);
    let (_, mut fiedlervector) = get_fiedler_vector(&coarsed_graph)?;

    println!("Refine");
    for (tmp_coarsed_graph, new_labels) in coarsed_data_iterator {
        println!("Refine to size {}", tmp_coarsed_graph.nrows());
        let mut fixed_values: Vec<Option<f64>> = vec![None; tmp_coarsed_graph.nrows()];
        for i in 0..labels.len() {
            let ind = labels[i];
            fixed_values[ind] = Some(fiedlervector[i]);
        }
        let mut interpolated_fieldervector = vec![0.0; tmp_coarsed_graph.nrows()];
        for i in 0..tmp_coarsed_graph.nrows() {
            if let Some(v) = fixed_values[i] {
                interpolated_fieldervector[i] = v;
                continue;
            }
            let row = tmp_coarsed_graph.get_row(i).unwrap();
            let mut value_counter = 0.0;
            let mut value = 0.0;
            let neighbor_slice = row.col_indices();
            for neighbor in neighbor_slice {
                if let Some(v) = fixed_values[*neighbor] {
                    value_counter += 1.0;
                    value += v;
                }
            }
            interpolated_fieldervector[i] = value / value_counter;
        }
        let int_fiedlervector = DVector::from(interpolated_fieldervector);
        let laplace = get_laplace_matrix(&tmp_coarsed_graph);
        fiedlervector = rqi(&laplace, &int_fiedlervector, 0.0001);
        labels = new_labels.clone();
    }
    println!("Final refine step");
    let mut fixed_values: Vec<Option<f64>> = vec![None; component_to_partition.graph.nrows()];
    for i in 0..labels.len() {
        let ind = labels[i];
        fixed_values[ind] = Some(fiedlervector[i]);
    }
    let mut interpolated_fieldervector = vec![0.0; component_to_partition.graph.nrows()];
    for i in 0..component_to_partition.graph.nrows() {
        if let Some(v) = fixed_values[i] {
            interpolated_fieldervector[i] = v;
            continue;
        }
        let row = component_to_partition.graph.get_row(i).unwrap();
        let mut value_counter = 0.0;
        let mut value = 0.0;
        let neighbor_slice = row.col_indices();
        for neighbor in neighbor_slice {
            if let Some(v) = fixed_values[*neighbor] {
                value_counter += 1.0;
                value += v;
            }
        }
        interpolated_fieldervector[i] = value / value_counter;
    }
    let fiedlervector = DVector::from(interpolated_fieldervector);
    let laplace = get_laplace_matrix(&component_to_partition.graph);
    let fiedlervector = rqi(&laplace, &fiedlervector, 0.0001);

    // calculate a seperator
    let seperator = {
        let mut curr_weight = 0.0;
        let mut vec = Vec::from_iter(fiedlervector.iter().cloned().enumerate());
        vec.sort_by(|(_, a), (_, b)| match a.partial_cmp(b) {
            Some(o) => o,
            None => std::cmp::Ordering::Equal,
        });
        let mut curr_seperator = f64::MIN;
        for (i, v) in vec {
            if curr_weight < new_target_weight {
                curr_weight += vertex_weights[i];
                curr_seperator = v;
            } else {
                break;
            }
        }
        curr_seperator
    };

    // calculate the initial partition
    let mut initial_partition: Vec<u32> = fiedlervector
        .iter()
        .map(|x| if *x < seperator { 1 } else { 0 })
        .collect();
    let mut initial_partition_weight = {
        let mut weight = 0.0;
        for i in 0..initial_partition.len() {
            if initial_partition[i] == 1 {
                weight += vertex_weights[i];
            }
        }
        weight
    };
    // move additional vertices until the target weight is reached
    if initial_partition_weight < new_target_weight {
        for (i, v) in fiedlervector.iter().enumerate() {
            if *v == seperator {
                initial_partition[i] = 1;
                initial_partition_weight += vertex_weights[i];
            }
            if initial_partition_weight >= new_target_weight {
                break;
            }
        }
    }
    let tmp_partition = apply_component_partition(
        graph_size,
        components_for_bipartition.components_in_subsection,
        &component_to_partition.vertex_ids,
        &initial_partition,
    );
    return Ok(tmp_partition);
}

#[derive(Debug)]
pub enum SpectralRCBError {
    TargetPartitionCountNotPowerOfTwo,
    GetEigenpairsLanczosError(GetEigenpairsLanczosError),
    PartitionGeometricError(PartitionGeometricError),
}

impl From<GetEigenpairsLanczosError> for SpectralRCBError {
    fn from(value: GetEigenpairsLanczosError) -> Self {
        return SpectralRCBError::GetEigenpairsLanczosError(value);
    }
}

impl From<PartitionGeometricError> for SpectralRCBError {
    fn from(value: PartitionGeometricError) -> Self {
        return SpectralRCBError::PartitionGeometricError(value);
    }
}

pub fn partition_spectral_rcb(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_partition_count: u32,
    n_eigenvectors: Option<u32>,
) -> Result<Vec<u32>, SpectralRCBError> {
    if !target_partition_count.is_power_of_two() {
        return Err(SpectralRCBError::TargetPartitionCountNotPowerOfTwo);
    }
    let n_eigenvectors = n_eigenvectors.unwrap_or(target_partition_count.ilog2());
    // ensure matrix is bigger than 30 entries
    let graph = if graph.nrows() < 30 {
        let mut construction_matrix = CooMatrix::new(30, 30);
        for (x, y, v) in graph.triplet_iter() {
            construction_matrix.push(x, y, *v);
        }
        CsrMatrix::from(&construction_matrix)
    } else {
        graph.clone()
    };
    let graph = get_laplace_matrix(&graph);
    let eigenpairs = get_eigenpairs_lanczos(&graph, n_eigenvectors as usize + 1)?;
    let eigenvectors = DMatrix::from_iterator(
        graph.nrows(),
        n_eigenvectors as usize,
        eigenpairs
            .iter()
            .skip(1)
            .map(|(_, v)| v)
            .map(|v| v.iter())
            .flatten()
            .cloned(),
    );
    let partition = partition_recursive_coordinate_bisection(
        &eigenvectors,
        &vertex_weights,
        target_partition_count,
    )?;

    Ok(partition)
}

pub fn partition_msb_matching_rb(
    ca: &CoarsenAlgorithm,
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_partition_count: u32,
) -> Result<Vec<u32>, SpectralRbError> {
    if !target_partition_count.is_power_of_two() {
        return Err(SpectralRbError::TargetPartitionCountNotPowerOfTwo);
    }

    let target_depth = u32::ilog2(target_partition_count);

    let partition = internal_msb_coarsen_rb(&ca, &graph, &vertex_weights, 0, target_depth)?;
    return Ok(partition);
}

fn internal_msb_coarsen_rb(
    ca: &CoarsenAlgorithm,
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    depth: u32,
    target_depth: u32,
) -> Result<Vec<u32>, SpectralBisectionError> {
    let initial_partition = msb_coarsen_bisection(&ca, &graph, &vertex_weights, None)?;
    let partition = if depth < target_depth - 1 {
        let (subgraph_a, vertex_weights_a) =
            get_sub_graph(&graph, &vertex_weights, &initial_partition, 0);
        let (subgraph_b, vertex_weights_b) =
            get_sub_graph(&graph, &vertex_weights, &initial_partition, 1);
        let partition_a =
            internal_msb_coarsen_rb(&ca, &subgraph_a, &vertex_weights_a, depth + 1, target_depth)?;
        let partition_b =
            internal_msb_coarsen_rb(&ca, &subgraph_b, &vertex_weights_b, depth + 1, target_depth)?;
        // combine the recursive partitions
        let mut tmp_partition: Vec<u32> = vec![0; initial_partition.len()];
        let mut partition_a_counter = 0;
        let mut partition_b_counter = 0;
        for i in 0..initial_partition.len() {
            if initial_partition[i] == 0 {
                tmp_partition[i] = partition_a[partition_a_counter];
                partition_a_counter += 1;
            } else {
                tmp_partition[i] = partition_b[partition_b_counter]
                    + ((2 as u32)
                        .pow(target_depth.abs_diff(depth).abs_diff(1).try_into().unwrap()));
                partition_b_counter += 1;
            }
        }
        tmp_partition
    } else {
        initial_partition
    };
    return Ok(partition);
}

pub fn msb_coarsen_bisection(
    ca: &CoarsenAlgorithm,
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_weight: Option<f64>,
) -> Result<Vec<u32>, SpectralBisectionError> {
    let total_weight: f64 = vertex_weights.iter().sum();
    let target_weight = target_weight.unwrap_or(total_weight / 2.0);
    let graph_size = graph.nrows();

    if target_weight < 0.0 || target_weight > total_weight {
        return Err(SpectralBisectionError::InvalidTargetWeight);
    }

    let components_for_bipartition =
        extract_components_for_partition(&graph, &vertex_weights, target_weight);
    let component_to_partition = &components_for_bipartition.component_to_partition;
    let new_target_weight = components_for_bipartition.new_target_weight;

    println!("Coarsen");
    let coarsed_data = match ca {
        CoarsenAlgorithm::FirstMatch {
            target_size,
            max_iterations,
        } => coarsen_first_matching(
            &component_to_partition.graph,
            &component_to_partition.vertex_weights,
            *target_size,
            *max_iterations,
        ),
        CoarsenAlgorithm::HeavyEdgeMatch {
            target_size,
            max_iterations,
        } => coarsen_heavy_edge_matching(
            &component_to_partition.graph,
            &component_to_partition.vertex_weights,
            *target_size,
            *max_iterations,
        ),
        CoarsenAlgorithm::HeavyEdgeMatch2Hop {
            target_size,
            max_iterations,
        } => coarsen_2hop_heavy_edge_matching(
            &component_to_partition.graph,
            &component_to_partition.vertex_weights,
            *target_size,
            *max_iterations,
        ),
    };

    let mut coarsed_data_iter = coarsed_data.iter();
    let component = coarsed_data_iter.next().unwrap();
    let coarsed_graph = &component.graph;

    if coarsed_graph.nrows() == 1 {
        println!("Coarsed graph has size 1, so use spectral bisection instead to avoid problems");
        return spectral_bisection(&graph, &vertex_weights, Some(target_weight));
    }

    // ensure matrix is bigger than 30 entries
    let coarsed_graph = if coarsed_graph.nrows() < 30 {
        let mut construction_matrix = CooMatrix::new(30, 30);
        for (x, y, v) in coarsed_graph.triplet_iter() {
            construction_matrix.push(x, y, *v);
        }
        CsrMatrix::from(&construction_matrix)
    } else {
        coarsed_graph.clone()
    };
    println!("Initial Partiton");
    let coarsed_graph = get_laplace_matrix(&coarsed_graph);
    let (_, mut fiedlervector) = get_fiedler_vector(&coarsed_graph)?;

    println!("Refine");
    for c in coarsed_data_iter {
        let interpolated_fiedler: Vec<f64> =
            c.labels.iter().map(|label| fiedlervector[*label]).collect();
        let interpolated_fiedler = DVector::from(interpolated_fiedler);
        let laplace = get_laplace_matrix(&c.graph);
        fiedlervector = rqi(&laplace, &interpolated_fiedler, 0.0001);
    }

    // calculate a seperator
    let seperator = {
        let mut curr_weight = 0.0;
        let mut vec = Vec::from_iter(fiedlervector.iter().cloned().enumerate());
        vec.sort_by(|(_, a), (_, b)| match a.partial_cmp(b) {
            Some(o) => o,
            None => std::cmp::Ordering::Equal,
        });
        let mut curr_seperator = f64::MIN;
        for (i, v) in vec {
            if curr_weight < new_target_weight {
                curr_weight += vertex_weights[i];
                curr_seperator = v;
            } else {
                break;
            }
        }
        curr_seperator
    };

    // calculate the initial partition
    let mut initial_partition: Vec<u32> = fiedlervector
        .iter()
        .map(|x| if *x < seperator { 1 } else { 0 })
        .collect();
    let mut initial_partition_weight = {
        let mut weight = 0.0;
        for i in 0..initial_partition.len() {
            if initial_partition[i] == 1 {
                weight += vertex_weights[i];
            }
        }
        weight
    };
    // move additional vertices until the target weight is reached
    if initial_partition_weight < new_target_weight {
        for (i, v) in fiedlervector.iter().enumerate() {
            if *v == seperator {
                initial_partition[i] = 1;
                initial_partition_weight += vertex_weights[i];
            }
            if initial_partition_weight >= new_target_weight {
                break;
            }
        }
    }
    let tmp_partition = apply_component_partition(
        graph_size,
        components_for_bipartition.components_in_subsection,
        &component_to_partition.vertex_ids,
        &initial_partition,
    );
    return Ok(tmp_partition);
}
