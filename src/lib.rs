use std::collections::HashSet;

use debug_print::debug_println;
use helpers::get_components_from_graph;
use nalgebra_sparse::CsrMatrix;

pub mod geometric;
pub mod growing;
pub mod helpers;
pub mod import;
pub mod iterativeimprovement;
pub mod multilevel;
pub mod random;
pub mod spectral;
pub mod stream;

pub struct Component {
    graph: CsrMatrix<f64>,
    vertex_weights: Vec<f64>,
    vertex_ids: HashSet<usize>,
}

pub struct ComponentsForBiPartition {
    component_to_partition: Component,
    components_in_subsection: Vec<Component>,
    new_target_weight: f64,
}

pub fn extract_components_for_partition(
    graph: &CsrMatrix<f64>,
    vertex_weights: &Vec<f64>,
    target_weight: f64,
) -> ComponentsForBiPartition {
    let mut components = get_components_from_graph(&graph, vertex_weights);
    if components.len() == 1 {
        debug_println!("[LIB] -> extract_components_for_partition:  graph is connected");
        return ComponentsForBiPartition {
            component_to_partition: components.pop().unwrap(),
            components_in_subsection: Vec::new(),
            new_target_weight: target_weight,
        };
    } else {
        debug_println!("[LIB] -> extract_components_for_partition:  graph is not connected");
        let mut left_components = Vec::new();
        let graph_size = graph.nrows();
        let mut left_weight = 0.0;
        while left_weight < target_weight {
            let c = match components.pop() {
                Some(c) => c,
                None => break,
            };
            let component_weight: f64 = c.vertex_weights.iter().sum();
            left_weight += component_weight;
            left_components.push(c);
        }
        let component_to_partition = left_components.pop().unwrap();
        let left_weight = left_weight - component_to_partition.vertex_weights.iter().sum::<f64>();
        let new_target_weight = target_weight - left_weight;
        debug_println!("[LIB] -> extract_components_for_partition: component to partition size {} instead of {} and new target weight {} = {} - {}", component_to_partition.graph.nrows(), graph_size, new_target_weight, target_weight, left_weight);
        return ComponentsForBiPartition {
            components_in_subsection: left_components,
            component_to_partition,
            new_target_weight,
        };
    }
}

pub fn apply_component_partition(
    graph_size: usize,
    components_in_subsection: Vec<Component>,
    component_vertex_ids: &HashSet<usize>,
    component_partition: &Vec<u32>,
) -> Vec<u32> {
    let mut tmp_partition = vec![0; graph_size];
    // move all component into on subsection
    for c in components_in_subsection {
        for index in c.vertex_ids.iter() {
            tmp_partition[*index] = 1;
        }
    }
    // apply partition of partitioned component
    let mut component_vertex_ids = Vec::from_iter(component_vertex_ids.iter().cloned());
    component_vertex_ids.sort();
    let mut counter = 0;
    for index in component_vertex_ids.iter() {
        if component_partition[counter] == 1 {
            tmp_partition[*index] = 1;
        }
        counter += 1;
    }
    tmp_partition
}
