use rand::seq::SliceRandom;

pub fn partition_random(partition_size: usize, target_partition_count: u32) -> Vec<u32> {
    let mut thread_rng = rand::thread_rng();
    let mut partition: Vec<u32> = vec![0; partition_size];
    for i in 0..partition_size {
        partition[i] = (i as u32) % target_partition_count;
    }
    partition.shuffle(&mut thread_rng);
    return partition;
}
