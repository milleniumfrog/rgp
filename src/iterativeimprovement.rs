use std::{cell::RefCell, collections::HashSet, fmt, rc::Rc};

use debug_print::debug_println;
use nalgebra_sparse::CsrMatrix;

use crate::helpers::get_edgecut_weight;

pub fn partition_kl(
    graph: &CsrMatrix<f64>,
    initial_partition: &Vec<u32>,
    max_iterations: usize,
) -> Vec<u32> {
    debug_println!("[ITERATIVE] -> KL: start");
    let mut partition = initial_partition.clone();

    if get_edgecut_weight(graph, &partition) == 0.0 {
        debug_println!("no edge cut found");
        return partition;
    }

    for i in 0..max_iterations {
        debug_println!("[ITERATIVE] -> KL: iteration {}", i);
        let mut group_a: HashSet<usize> = (0..partition.len())
            .filter(|i| partition[*i] == 0)
            .collect();
        let mut group_b: HashSet<usize> = (0..partition.len())
            .filter(|i| partition[*i] != 0)
            .collect();
        // compute d values
        let mut d_values: Vec<f64> = {
            let mut tmp_dvalues: Vec<f64> = (0..partition.len()).map(|_x| 0.0).collect();
            for i in group_a.iter() {
                let row = graph.get_row(*i).unwrap();
                for j in row.col_indices() {
                    if group_a.contains(j) {
                        tmp_dvalues[*i] -= graph.get_entry(*i, *j).unwrap().into_value();
                    } else if group_b.contains(j) {
                        tmp_dvalues[*i] += graph.get_entry(*i, *j).unwrap().into_value();
                    }
                }
            }
            for i in group_b.iter() {
                let row = graph.get_row(*i).unwrap();
                for j in row.col_indices() {
                    if group_b.contains(j) {
                        tmp_dvalues[*i] -= graph.get_entry(*i, *j).unwrap().into_value();
                    } else if group_a.contains(j) {
                        tmp_dvalues[*i] += graph.get_entry(*i, *j).unwrap().into_value();
                    }
                }
            }
            tmp_dvalues
        };
        let mut triple_store: Vec<(f64, usize, usize)> = Vec::new();
        let mut negative_g_counter: usize = 0;
        for _iter in 0..usize::min(group_a.len(), group_b.len()) {
            let mut curr_g_max_triple: (f64, usize, usize) = (f64::MIN, 0, 0);
            for a in group_a.iter() {
                for b in group_b.iter() {
                    let g = match graph.get_entry(*a, *b) {
                        None => d_values[*a] + d_values[*b],
                        Some(c) => d_values[*a] + d_values[*b] - (2.0 * c.into_value()),
                    };
                    if g > curr_g_max_triple.0 {
                        curr_g_max_triple = (g, *a, *b);
                    }
                }
            }
            if curr_g_max_triple.0 <= 0.0 {
                negative_g_counter += 1;
            } else {
                negative_g_counter = 0;
            }
            triple_store.push(curr_g_max_triple);
            if negative_g_counter > 50 {
                break;
            }
            triple_store.push(curr_g_max_triple);
            // remove a and b from A and B
            group_a.remove(&curr_g_max_triple.1);
            group_b.remove(&curr_g_max_triple.2);
            // update dvalues
            let row_a = graph.get_row(curr_g_max_triple.1).unwrap();
            let row_b = graph.get_row(curr_g_max_triple.2).unwrap();
            let nodes_to_change = row_a.col_indices().iter().chain(row_b.col_indices().iter());
            for node in nodes_to_change {
                d_values[*node] = 0.0;
                let row = graph.get_row(*node).unwrap();
                let group = partition[*node];
                for j in row.col_indices() {
                    if group == 0 {
                        if group_a.contains(j) {
                            d_values[*node] -= graph.get_entry(*node, *j).unwrap().into_value();
                        } else if group_b.contains(j) {
                            d_values[*node] += graph.get_entry(*node, *j).unwrap().into_value();
                        }
                    } else {
                        if group_b.contains(j) {
                            d_values[*node] -= graph.get_entry(*node, *j).unwrap().into_value();
                        } else if group_a.contains(j) {
                            d_values[*node] += graph.get_entry(*node, *j).unwrap().into_value();
                        }
                    }
                }
            }
        }
        let mut k = 0;
        for i in 0..triple_store.len() {
            if triple_store[i].0 <= 0.0 {
                k = i;
                break;
            }
        }
        for (_v, i, j) in &triple_store.as_slice()[0..k] {
            group_a.remove(i);
            group_b.remove(j);
            group_a.insert(*j);
            group_b.insert(*i);
            partition[*i] = 1;
            partition[*j] = 0;
        }
        if k == 0 {
            break;
        }
    }

    return partition;
}

pub fn partition_bkl(
    graph: &CsrMatrix<f64>,
    initial_partition: &Vec<u32>,
    max_iterations: usize,
) -> Vec<u32> {
    debug_println!("[ITERATIVE] BKL");
    let mut partition = initial_partition.clone();

    if get_edgecut_weight(graph, &partition) == 0.0 {
        debug_println!("no edge cut found");
        return partition;
    }

    for i in 0..max_iterations {
        debug_println!("[ITERATIVE] -> BKL: iteration {}", i);
        let mut group_a: HashSet<usize> = (0..partition.len())
            .filter(|i| partition[*i] == 0)
            .collect();
        let mut group_b: HashSet<usize> = (0..partition.len())
            .filter(|i| partition[*i] != 0)
            .collect();
        let nodes_in_border: HashSet<usize> = {
            let mut set: HashSet<usize> = HashSet::new();
            for (i, j, _v) in graph.triplet_iter() {
                if group_a.contains(&i) && group_b.contains(&j)
                    || group_a.contains(&j) && group_b.contains(&i)
                {
                    set.insert(i);
                    set.insert(j);
                }
            }
            set
        };
        let mut group_a_border: HashSet<usize> =
            group_a.intersection(&nodes_in_border).cloned().collect();
        let mut group_b_border: HashSet<usize> =
            group_b.intersection(&nodes_in_border).cloned().collect();

        let mut d_values: Vec<f64> = {
            let mut tmp_dvalues: Vec<f64> = (0..partition.len()).map(|_x| 0.0).collect();
            for i in group_a_border.iter() {
                let row = graph.get_row(*i).unwrap();
                for j in row.col_indices() {
                    if group_a.contains(j) {
                        tmp_dvalues[*i] -= graph.get_entry(*i, *j).unwrap().into_value();
                    } else if group_b.contains(j) {
                        tmp_dvalues[*i] += graph.get_entry(*i, *j).unwrap().into_value();
                    }
                }
            }
            for i in group_b_border.iter() {
                let row = graph.get_row(*i).unwrap();
                for j in row.col_indices() {
                    if group_b.contains(j) {
                        tmp_dvalues[*i] -= graph.get_entry(*i, *j).unwrap().into_value();
                    } else if group_a.contains(j) {
                        tmp_dvalues[*i] += graph.get_entry(*i, *j).unwrap().into_value();
                    }
                }
            }
            tmp_dvalues
        };

        let mut triple_store: Vec<(f64, usize, usize)> = Vec::new();

        let mut negative_g_counter: usize = 0;

        for _ in 0..usize::min(group_a_border.len(), group_b_border.len()) {
            let mut curr_g_max_triple: (f64, usize, usize) = (f64::MIN, 0, 0);
            for a in group_a_border.iter() {
                for b in group_b_border.iter() {
                    let g = match graph.get_entry(*a, *b) {
                        None => d_values[*a] + d_values[*b],
                        Some(c) => d_values[*a] + d_values[*b] - (2.0 * c.into_value()),
                    };
                    if g > curr_g_max_triple.0 {
                        curr_g_max_triple = (g, *a, *b);
                    }
                }
            }
            if curr_g_max_triple.0 <= 0.0 {
                negative_g_counter += 1;
            } else {
                negative_g_counter = 0;
            }
            triple_store.push(curr_g_max_triple);
            if negative_g_counter > 50 {
                break;
            }
            // remove a and b from A and B
            group_a_border.remove(&curr_g_max_triple.1);
            group_b_border.remove(&curr_g_max_triple.2);
            group_a.remove(&curr_g_max_triple.1);
            group_b.remove(&curr_g_max_triple.2);
            // update dvalues
            let row_a = graph.get_row(curr_g_max_triple.1).unwrap();
            let row_b = graph.get_row(curr_g_max_triple.2).unwrap();
            let nodes_to_change = row_a.col_indices().iter().chain(row_b.col_indices().iter());
            for node in nodes_to_change {
                d_values[*node] = 0.0;
                let row = graph.get_row(*node).unwrap();
                let group = partition[*node];
                for j in row.col_indices() {
                    if group == 0 {
                        if group_a.contains(j) {
                            d_values[*node] -= graph.get_entry(*node, *j).unwrap().into_value();
                        } else if group_b.contains(j) {
                            d_values[*node] += graph.get_entry(*node, *j).unwrap().into_value();
                        }
                    } else {
                        if group_b.contains(j) {
                            d_values[*node] -= graph.get_entry(*node, *j).unwrap().into_value();
                        } else if group_a.contains(j) {
                            d_values[*node] += graph.get_entry(*node, *j).unwrap().into_value();
                        }
                    }
                }
            }
        }
        let mut k = 0;
        for i in 0..triple_store.len() {
            if triple_store[i].0 <= 0.0 {
                k = i;
                break;
            }
        }
        for (_v, i, j) in &triple_store.as_slice()[0..k] {
            group_a.remove(i);
            group_b.remove(j);
            group_a_border.remove(i);
            group_a_border.remove(j);
            group_a.insert(*j);
            group_a_border.insert(*j);
            group_b.insert(*i);
            group_b_border.insert(*i);
            partition[*i] = 1;
            partition[*j] = 0;
        }
        if k == 0 {
            break;
        }
    }

    return partition;
}

pub struct FMItem {
    pub node: usize,
    pub gain_index: RefCell<usize>,
    pub before: RefCell<Option<Rc<FMItem>>>,
    pub after: RefCell<Option<Rc<FMItem>>>,
}

impl fmt::Debug for FMItem {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("FMItem")
            .field("node", &self.node)
            .field("gain_index", &self.gain_index)
            .field("before", &self.before.borrow().is_some())
            .field("after", &self.after.borrow().is_some())
            .finish()
    }
}

pub fn partition_fm(
    graph: &CsrMatrix<f64>,
    initial_partition: &Vec<u32>,
    vertex_weights: &Vec<f64>,
) -> Vec<u32> {
    let vertex_weights: Vec<usize> = vertex_weights.iter().map(|x| *x as usize).collect();
    let mut partition = initial_partition.clone();
    let mut group_a: HashSet<usize> = (0..partition.len())
        .filter(|i| partition[*i] == 0)
        .collect();
    let mut group_b: HashSet<usize> = (0..partition.len())
        .filter(|i| partition[*i] != 0)
        .collect();

    if get_edgecut_weight(graph, &partition) == 0.0 {
        return partition;
    }

    let initial_gains: Vec<i32> = {
        let mut tmp: Vec<i32> = (0..initial_partition.len()).map(|_x| 0).collect();
        for i in 0..initial_partition.len() {
            let row = graph.get_row(i).unwrap();
            for j in row.col_indices() {
                if (group_a.contains(&i) && group_b.contains(&j))
                    || (group_a.contains(&j) && group_b.contains(&i))
                {
                    tmp[i] += 1;
                } else {
                    tmp[i] -= 1;
                }
            }
        }
        tmp
    };

    let max_gain = initial_gains.iter().max().unwrap();
    let min_gain = initial_gains.iter().min().unwrap();

    let mut bucket_a: Vec<Option<Rc<FMItem>>> =
        (0..(max_gain - min_gain + 1)).map(|_x| None).collect();
    let mut bucket_b: Vec<Option<Rc<FMItem>>> =
        (0..(max_gain - min_gain + 1)).map(|_x| None).collect();

    let fm_item_list: Vec<Rc<FMItem>> = (0..initial_gains.len())
        .map(|i| {
            Rc::new(FMItem {
                node: i,
                before: RefCell::new(None),
                after: RefCell::new(None),
                gain_index: RefCell::new((initial_gains[i] - min_gain) as usize),
            })
        })
        .collect();

    for i in 0..initial_gains.len() {
        let index = (initial_gains[i] - min_gain) as usize;
        let fm_item = Rc::clone(&fm_item_list[i]);
        if group_a.contains(&i) {
            match &bucket_a[index] {
                None => bucket_a[index] = Some(Rc::clone(&fm_item)),
                Some(other_fm_item) => {
                    other_fm_item.before.replace(Some(Rc::clone(&fm_item)));
                    fm_item.after.replace(Some(Rc::clone(&other_fm_item)));
                    bucket_a[index] = Some(Rc::clone(&fm_item));
                }
            }
        } else if group_b.contains(&i) {
            match &bucket_b[index] {
                None => bucket_b[index] = Some(Rc::clone(&fm_item)),
                Some(other_fm_item) => {
                    other_fm_item.before.replace(Some(Rc::clone(&fm_item)));
                    fm_item.after.replace(Some(Rc::clone(&other_fm_item)));
                    bucket_b[index] = Some(Rc::clone(&fm_item));
                }
            }
        }
    }

    let mut iteration_store: Vec<(usize, i32)> = Vec::new();

    let max_iterations = usize::min(group_a.len(), group_b.len());

    let mut moved_nodes: HashSet<usize> = HashSet::new();

    let mut edge_cut = {
        let mut cut = 0;
        for (i, j, _v) in graph.triplet_iter() {
            if (group_a.contains(&i) && group_b.contains(&j))
                || (group_a.contains(&j) && group_b.contains(&i))
            {
                cut += 1;
            }
        }
        cut
    };

    let mut leq_null_counter = (0, 0);

    for i in 0..2 * max_iterations {
        // early break if rebalance is activated
        if i == 20000 {
            break;
        }
        let bucket_to_use: &mut Vec<Option<Rc<FMItem>>> = if i % 2 == 0 {
            bucket_a.as_mut()
        } else {
            bucket_b.as_mut()
        };
        let max_gain_fm_item_list: Vec<Rc<FMItem>> = bucket_to_use
            .iter()
            .rev()
            .filter_map(|x| x.clone())
            .collect();
        if max_gain_fm_item_list.len() > 0 {
            let max_gain_fm_item = max_gain_fm_item_list.first().unwrap();
            if (*max_gain_fm_item.gain_index.borrow() as i32) + min_gain <= 0 {
                if i % 2 == 0 {
                    leq_null_counter.0 += 1;
                } else {
                    leq_null_counter.1 += 1;
                }
            } else {
                if i % 2 == 0 {
                    leq_null_counter.0 = 0;
                } else {
                    leq_null_counter.1 = 0;
                }
            }
            if leq_null_counter.0 > 50 || leq_null_counter.1 > 50 {
                // debug_println!("Stop early because of 50 Steps without increasing the gain");
                break;
            }
            match *max_gain_fm_item.after.borrow() {
                None => {
                    bucket_to_use[*max_gain_fm_item.gain_index.borrow()] = None;
                }
                Some(ref after) => {
                    bucket_to_use[*max_gain_fm_item.gain_index.borrow()] = Some(Rc::clone(after));
                    after.before.replace(None);
                }
            }
            let group_to_pull = if i % 2 == 0 {
                &mut group_a
            } else {
                &mut group_b
            };
            group_to_pull.remove(&max_gain_fm_item.node);
            let group_to_push = if i % 2 == 0 {
                &mut group_b
            } else {
                &mut group_a
            };
            group_to_push.insert(max_gain_fm_item.node);
            edge_cut -= (*max_gain_fm_item.gain_index.borrow()) as i32 + min_gain;
            iteration_store.push((max_gain_fm_item.node, edge_cut));
            moved_nodes.insert(max_gain_fm_item.node);
            // rebalance the gains of the neighbors
            let row = graph.get_row(max_gain_fm_item.node).unwrap();
            for neighbor_index in row.col_indices() {
                if moved_nodes.contains(&neighbor_index) {
                    continue;
                }
                let neighbor_fm_item = Rc::clone(&fm_item_list[*neighbor_index]);
                let old_gain = (*neighbor_fm_item.gain_index.borrow() as i32) + min_gain;
                let new_gain = {
                    let neighbor_row = graph.get_row(*neighbor_index).unwrap();
                    let mut tmp = 0;
                    for j in neighbor_row.col_indices() {
                        if (group_a.contains(&neighbor_index) && group_b.contains(&j))
                            || (group_a.contains(&j) && group_b.contains(&neighbor_index))
                        {
                            tmp += 1;
                        } else {
                            tmp -= 1;
                        }
                    }
                    tmp
                };
                if new_gain == old_gain {
                    continue;
                }
                let bucket_to_use: &mut Vec<Option<Rc<FMItem>>> =
                    if group_a.contains(neighbor_index) {
                        bucket_a.as_mut()
                    } else {
                        bucket_b.as_mut()
                    };
                match neighbor_fm_item.before.borrow().clone() {
                    None => match neighbor_fm_item.after.borrow().clone() {
                        None => bucket_to_use[(old_gain - min_gain) as usize] = None,
                        Some(ref a) => {
                            bucket_to_use[(old_gain - min_gain) as usize] = Some(Rc::clone(a));
                        }
                    },
                    Some(ref b) => match neighbor_fm_item.after.borrow().clone() {
                        None => {
                            b.after.replace(None);
                        }
                        Some(ref a) => {
                            b.after.replace(Some(Rc::clone(a)));
                        }
                    },
                };
            }
        } else {
            break;
        }
    }

    let mut min_edge_cut = i32::MAX;
    let mut min_edge_cut_index = 0;

    for k in 0..iteration_store.len() {
        let (_node, edge_cut) = iteration_store[k];
        if edge_cut < min_edge_cut {
            min_edge_cut = edge_cut;
            min_edge_cut_index = k;
        }
    }

    let swapped_nodes: HashSet<usize> = HashSet::from_iter(
        iteration_store.as_slice()[0..min_edge_cut_index]
            .iter()
            .map(|(x, _)| *x),
    );

    for i in 0..partition.len() {
        if swapped_nodes.contains(&i) {
            partition[i] = if partition[i] == 0 { 1 } else { 0 };
        }
    }

    return partition;
}
