use bindgen::Builder;
use std::{
    env,
    path::PathBuf,
    process::{Command, Stdio},
};

/// Builds the gklib and metis c repositories with make and
/// generates the rust - c bridge with bindgen
///
/// tested on mac
fn main() {
    // the output path for metis-sys in the target folder
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    {
        println!("Configure gklib with make.");
        let mut config_gklib_process = Command::new("make")
            .current_dir("./gklib")
            .args(["config", &format!("prefix={}/gklib", out_path.display())])
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .spawn()
            .unwrap();
        config_gklib_process
            .wait()
            .expect("Waiting for gklibs 'make config' failed.");
        println!("Install gklib with make.");
        let mut install_gklib_process = Command::new("make")
            .current_dir("./gklib")
            .args(["install"])
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .spawn()
            .unwrap();
        install_gklib_process
            .wait()
            .expect("Waiting for gklibs 'make install' failed.");
    }
    {
        println!("Configure metis with make.");
        let mut config_metis_process = Command::new("make")
            .args([
                "config",
                &format!("prefix={}/metis", out_path.display()),
                &format!("gklib_path={}/gklib", out_path.display()),
            ])
            .current_dir("./metis")
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .spawn()
            .unwrap();
        config_metis_process
            .wait()
            .expect("Waiting for metis 'make config' failed.");
        println!("Install metis with make.");
        let mut install_metis_process = Command::new("make")
            .args(["install"])
            .current_dir("./metis")
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .spawn()
            .unwrap();
        install_metis_process
            .wait()
            .expect("Waiting for metis 'make install' failed.");
    }
    {
        println!("Generate rust types with cbindgen");
        let metis_bindings = Builder::default()
            .header(&format!("{}/metis/include/metis.h", out_path.display()))
            .generate()
            .expect("Could not generate metis bindings.");
        println!("Write rust types to file");
        metis_bindings
            .write_to_file(out_path.join("metis.rs"))
            .expect("Could not write metis bindings!");
    }
    println!("Log rustc comments for linking and including");
    println!(
        "cargo:rustc-link-search=native={}/metis/lib",
        out_path.display()
    );
    println!(
        "cargo:rustc-link-search=native={}/gklib/lib",
        out_path.display()
    );
    println!("cargo:rustc-link-lib=static=metis");
    println!("cargo:rustc-link-lib=static=GKLib");

    // register rerun flags
    println!("cargo:rerun-if-changed=src/lib.rs");
}
