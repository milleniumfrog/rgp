#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/metis.rs"));

use nalgebra_sparse::CsrMatrix;

/// Wraps metis recursive bisection with
/// preconfiguration for paritioning
/// of graphs with one constraint
pub fn partition_metis_rb(
    graph_as_sparse_adj_matrix: &CsrMatrix<i32>,
    target_partition_count: u32,
) -> Vec<u32> {
    let graph_matrix = graph_as_sparse_adj_matrix;

    let mut number_of_vertices: i32 = graph_matrix.nrows().try_into().unwrap();
    let mut number_of_constraints: i32 = 1;
    let (raw_csr_matrix_row_offsets, raw_csr_matrix_col_indices, _raw_edge_weights) =
        graph_matrix.csr_data();
    let mut xadj: Vec<i32> = raw_csr_matrix_row_offsets
        .iter()
        .map(|x| *x as i32)
        .collect();
    let mut adjncy: Vec<i32> = raw_csr_matrix_col_indices
        .iter()
        .map(|x| *x as i32)
        .collect();

    let mut options: Vec<i32> = vec![0; METIS_NOPTIONS as usize];

    unsafe {
        METIS_SetDefaultOptions(options.as_mut_ptr());
    }

    options[moptions_et_METIS_OPTION_OBJTYPE as usize] = mobjtype_et_METIS_OBJTYPE_CUT as i32;
    options[moptions_et_METIS_OPTION_PTYPE as usize] = mptype_et_METIS_PTYPE_RB as i32;
    options[moptions_et_METIS_OPTION_DBGLVL as usize] = 2;

    // Upon successful completion, this variable stores the edge-cut or the
    // total communication volume of the partitioning solution. The value
    // returned depends on the partitioning’s objective function.
    let mut obj_value_box = Box::new(0);
    let obj_value: *mut i32 = &mut *obj_value_box;

    let mut n_parts_box = Box::new(target_partition_count as i32);
    let n_parts: *mut i32 = &mut *n_parts_box;

    // This is a vector of size nvtxs that upon successful completion stores
    // the partition vector of the graph. The numbering of this vector starts
    // from either 0 or 1, depending on the value of options[METIS OPTION NUMBERING].
    let mut part: Vec<i32> = vec![0; graph_matrix.nrows()];

    unsafe {
        METIS_PartGraphRecursive(
            &mut number_of_vertices,
            &mut number_of_constraints,
            xadj.as_mut_ptr(),
            adjncy.as_mut_ptr(),
            std::ptr::null_mut(),
            std::ptr::null_mut(),
            std::ptr::null_mut(),
            n_parts,
            std::ptr::null_mut(),
            std::ptr::null_mut(),
            options.as_mut_ptr(),
            obj_value,
            part.as_mut_ptr(),
        )
    };
    let part = part.iter().map(|x| *x as u32).collect();
    return part;
}

/// Wraps metis kway partitioning with
/// preconfiguration for paritioning
/// of graphs with one constraint
pub fn partition_metis_kway(
    graph_as_sparse_adj_matrix: &CsrMatrix<i32>,
    target_partition_count: u32,
) -> Vec<u32> {
    let graph_matrix = graph_as_sparse_adj_matrix;

    let mut number_of_vertices: i32 = graph_matrix.nrows().try_into().unwrap();
    let mut number_of_constraints: i32 = 1;
    let (raw_csr_matrix_row_offsets, raw_csr_matrix_col_indices, _raw_edge_weights) =
        graph_matrix.csr_data();
    let mut xadj: Vec<i32> = raw_csr_matrix_row_offsets
        .iter()
        .map(|x| *x as i32)
        .collect();
    let mut adjncy: Vec<i32> = raw_csr_matrix_col_indices
        .iter()
        .map(|x| *x as i32)
        .collect();

    let mut options: Vec<i32> = vec![0; METIS_NOPTIONS as usize];

    unsafe {
        METIS_SetDefaultOptions(options.as_mut_ptr());
    }

    options[moptions_et_METIS_OPTION_OBJTYPE as usize] = mobjtype_et_METIS_OBJTYPE_CUT as i32;
    options[moptions_et_METIS_OPTION_PTYPE as usize] = mptype_et_METIS_PTYPE_KWAY as i32;
    options[moptions_et_METIS_OPTION_DBGLVL as usize] = 2;

    // Upon successful completion, this variable stores the edge-cut or the
    // total communication volume of the partitioning solution. The value
    // returned depends on the partitioning’s objective function.
    let mut obj_value_box = Box::new(0);
    let obj_value: *mut i32 = &mut *obj_value_box;

    let mut n_parts_box = Box::new(target_partition_count as i32);
    let n_parts: *mut i32 = &mut *n_parts_box;

    // This is a vector of size nvtxs that upon successful completion stores
    // the partition vector of the graph. The numbering of this vector starts
    // from either 0 or 1, depending on the value of options[METIS OPTION NUMBERING].
    let mut part: Vec<i32> = vec![0; graph_matrix.nrows()];

    unsafe {
        METIS_PartGraphKway(
            &mut number_of_vertices,
            &mut number_of_constraints,
            xadj.as_mut_ptr(),
            adjncy.as_mut_ptr(),
            std::ptr::null_mut(),
            std::ptr::null_mut(),
            std::ptr::null_mut(),
            n_parts,
            std::ptr::null_mut(),
            std::ptr::null_mut(),
            options.as_mut_ptr(),
            obj_value,
            part.as_mut_ptr(),
        )
    };
    let part = part.iter().map(|x| *x as u32).collect();
    return part;
}
